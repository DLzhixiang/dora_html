var webpack = require('webpack')
var precss = require('precss');
var autoprefixer = require('autoprefixer');
var HtmlWebpackPlugin = require('html-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var pxtorem = require('postcss-pxtorem');
var path = require('path')

const svgDirs = [
  require.resolve('antd-mobile').replace(/warn\.js$/, ''),  // 1. 属于 antd-mobile 内置 svg 文件
  // path.resolve(__dirname, 'src/my-project-svg-foler'),  // 2. 自己私人的 svg 存放目录
];

var config = {
  context: path.join(__dirname, './src'),
  entry: {
    bundle: [
      // 'whatwg-fetch',
      // 'pdfjs-dist/build/pdf.worker.entry',
      './index.js',
    ],
    vendor: [
      'axios',
      'fastclick',
      'ityped',
      'jquery',
      'jquery-mousewheel',
      'lodash',
      'malihu-custom-scrollbar-plugin',
      'mobx',
      'mobx-react',
      'nprogress',
      'progressbar.js',
      'qrcode.react',
      'react',
      'react-addons-css-transition-group',
      'react-custom-scrollbars',
      'react-dom',
      'react-helmet',
      'react-modal',
      'react-motion',
      'react-router',
      'react-scrollbar',
      'react-slick',
      'store',
      'velocity-react',
      'whatwg-fetch',
      'classnames'
    ]
  },
  output: {
    path: path.join(__dirname, "dist"),
    publicPath: "/",
    filename: "bundle.[chunkhash:8].js",
    chunkFilename: '[chunkhash:8].chunk.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader?cacheDirectory',
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract(['css-loader','postcss-loader']),
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(['css-loader?modules&localIdentName=[hash:base64:5]','postcss-loader','sass-loader']),
      },
      {
        test: /\.(jpe?g|png|mp4)$/,
        loader: 'url-loader?limit=2048',
      },
      {
        test: /\.html?$/,
        loader: 'html-loader',
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      }, {
        test: /\.(ttf|otf)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      }, {
        test: /\.(svg)$/i,
        loader: 'svg-sprite-loader',
        include: svgDirs,  // 把 svgDirs 路径下的所有 svg 文件交给 svg-sprite-loader 插件处理
      },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
    // new webpack.optimize.AggressiveSplittingPlugin({
		// 	minSize: 30000,
		// 	maxSize: 50000
		// }),
    new webpack.LoaderOptionsPlugin({
      test: /\.(scss|css)$/,
      debug: false,
      minimize: true,
      options: {
        context: __dirname,
        postcss: function() {
            return [
              precss,
              // pxtorem({ rootValue: 100, propWhiteList: [] }),
              autoprefixer({
                browsers: ['last 2 versions', 'Firefox ESR', '> 1%', 'ie >= 8', 'iOS >= 8', 'Android >= 4'],
              })
            ];
        },
        // context: path.join(__dirname, 'src'),
        // output: {
        //     path: path.join(__dirname, 'dist')
        // }
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: "vendor",
        minChunks: Infinity,
        filename: '[chunkhash:8].vendor.js'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'mainifest',
      // chunks: ['vendor'],
      filename: '[hash:8].mainifest.js'
    }),
		new webpack.optimize.UglifyJsPlugin({
      // 最紧凑的输出
      beautify: false,
      // 删除所有的注释
      comments: false,
      compressor: {
        // 在UglifyJs删除没有用到的代码时不输出警告
        warnings: false,
        // 删除所有的 `console` 语句
        // 还可以兼容ie浏览器
        // drop_console: true,
        // 内嵌定义了但是只用到一次的变量
        // collapse_vars: true,
        // 提取出出现多次但是没有定义成变量去引用的静态值
        // reduce_vars: true,
      }
    }),
    new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|ko|ja|zh-cn)$/),
    new ExtractTextPlugin({ filename: '[name]-[contenthash:8].css', allChunks: true }),
    new HtmlWebpackPlugin({ hash: true, template: './index.ejs', minimize: true})
  ],
  // recordsOutputPath: path.join(__dirname, 'js', 'records.json'),
  resolve: {
    extensions: ['.web.js', '.js', '.json'],
  },
}

module.exports = config;
