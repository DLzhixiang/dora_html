
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from "mobx-react";
import { Router, browserHistory, hashHistory } from 'react-router';
// import 'moment/locale/zh-cn';
// import DevTools from 'mobx-react-devtools';
import 'antd/dist/antd.css';
import FastClick from 'fastclick';
FastClick.attach(document.body);

import Duola from './stores';
import RouterConfig from './routes';
const duola = new Duola()

ReactDOM.render(
  <Provider duola={duola}>
    <Router
      routes={RouterConfig} history={hashHistory}
    >
    </Router>
  </Provider>,
  document.getElementById('root')
)

//onUpdate={() => window.scrollTo(0, 1)}
