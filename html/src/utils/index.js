
//cookie  document.cookie="name=value;domain=.pencilnews.cn";

const TOKEN_NAME = 'b_shufan_token'
export const delCookie = (name)=> {
  const exp = new Date();
  exp.setTime(exp.getTime() - 1);
  const cval = getCookie(name);
  if(cval != null)
  document.cookie= `${name}=${cval};expires=${exp.toGMTString()}; path=/`; //name + "="+cval+";expires="+exp.toGMTString()+"; path=/";
}

export const getCookie = (name) => {
  let arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)")
  if (arr = document.cookie.match(reg)){
    return unescape(arr[2])
  }else{
    return null
  }
}

export const setCookie = (name,value) =>{
  document.cookie= `${name}=${value}; path=/`;
}

export const setToken = (token) => setCookie(TOKEN_NAME,token)
export const getToken = () => getCookie(TOKEN_NAME)
export const removeToken = () => setCookie(TOKEN_NAME,'')

//obj to query
export const obj2Query = (obj) => Object.keys(obj).filter(t => obj[t] != undefined)
  .map(t => encodeURIComponent(t) + '=' + encodeURIComponent(obj[t]))
  .join('&');



export const PIXEL = document.documentElement.style.fontSize.replace('px','') / 100;

let u;
export const isiOS = ()=> {
  u = navigator.userAgent;
  return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
}

export const isAndroid = ()=> {
  u = navigator.userAgent;
  return u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
}

export const trim = str => str.replace(/^(\s|\u00A0)+/,'').replace(/(\s|\u00A0)+$/,'')

const reg = /^[1-9]+[0-9]*]*$/ ; //判断字符串是否为数字 /^[0-9]+.?[0-9]*$/ //判断正整数 /^[1-9]+[0-9]*]*$/
export const isNumer = value => reg.test(value)

export const bytesToSize = (bytes) => {
  if (bytes === 0) return '0 B';
   const k = 1024;
   const sizes = ['B','KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   let i = Math.floor(Math.log(bytes) / Math.log(k));
   return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];//toPrecision(3) 后面保留一位小数，如1.0GB
}

export const clipPath = (path, length) => {
  if(path.length < length)return path;
  let newPath = "";
  let re = new RegExp(`(.{${length-6}})(.)*(.{6})`);
  newPath = path.replace(re, "$1...$3");
  return newPath;
}

export const getFileTypeByName = (name) => {
    let re = /(?:\.([^.]+))?$/;
    return re.exec(name)[1]
}


export const shuffle = (arr) => {
    let newArr = [];
    arr.reduce((curArr)=>{
      let index = Math.floor(Math.random() * curArr.length);
      newArr.push(curArr[index]);
      return curArr.filter((val,ind)=>(index != ind))
    },arr)
    return newArr
}

