
import App from './containers/App'

import home from './containers/Home'
import help from './containers/Help'
import print from './containers/Print'
import error from './containers/Error'
import success from './containers/Success'
import welcome from './containers/Welcome'
import usbdrive from './containers/USBDrive'
import guide from './containers/Guide'
import pay from './containers/Pay'
import test from './containers/Test'
import login from './containers/Login'
import waiting from './containers/Waiting'
import printerlist from './containers/PrinterList'
import loglist from './containers/Log'
import feedback from './containers/FeedBack'
import operation from './containers/Operation'
import refund from './containers/Refund'

// const preview = (location, callback) => {
//   System.import('./containers/Preview').then(component => {
//       callback(null, component.default || component)
//   })
// }


const config = [
  {
    path: '/',
    component: App,
    indexRoute:{ component: home },
    childRoutes:[
      { path: '/help', component: help },
      { path: '/print', component: print },
      { path: '/error', component: error },
      { path: '/success', component: success },
      { path: '/welcome', component: welcome },
      { path: '/usbdrive', component: usbdrive },
      { path: '/guideout', component: guide },
      { path: '/guidein', component: guide },
      { path: '/pay', component: pay },
      { path: '/test', component: test },
      { path: '/login', component: login },
      { path: '/waiting', component: waiting },
      { path: '/printerlist', component: printerlist },
      { path: '/loglist', component: loglist },
      { path: '/feedback', component: feedback },
      { path: '/operation', component: operation },
      { path: '/refund', component: refund },
      { path: '*', component: home },
    ]
  }
]

export default config
