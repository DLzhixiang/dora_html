
export const apis = {
  API_URL: 'http://api.duoladayin.com/',//生产服务器
  SOCKET_URL: 'http://47.92.151.203:3001/',//socket服务器地址
  // API_URL: 'http://47.92.134.29/',//测试服务器
  // SOCKET_URL: 'http://47.92.151.203:4001/',//socket测试服务器地址
  TERMINAL_PATH: 'v2/terminal/',//终端检查轮训地址
  login: 'terminal/login',                //登录
  printers: 'terminal/printer',           //获取打印机列表
  verifycode: 'terminal/verifycode',      //验证打印码
  printstatus: 'terminal/printstatus',      //标记打印任务
  devicestatus: 'terminal/device/status',      //轮询上报打印机设备信息
  scanstatus: 'terminal/scan/status',      //扫描头信息上报
  userinfo: 'terminal/info',      //获取用户信息
  submit: 'v2/order/udiskSubmit',    //获取订单信息
  orderStatus: 'order/getOrder/',   //获取支付状态
  terminalStatus: 'terminal/status', //上传终端信息至运维服务器
  obtainStatusByPhone: 'status',//获取扫码登录状态
  getCode: 'getCode2Terminal',//获取扫描二维码
  getTerminalGroup: 'getTermGroup',//获取设备分组,
  getVersion: '',//获取当前版本号
  feedback: 'v2/feedback/printFeedback',//打印完成反馈
  setPaperCount: 'v2/terminal/setPaperCount',//更新纸张数
  getThemeUsed: 'v2/theme/getThemeUsed',//获取设备样式 所在终端分组
  getPaperCount: 'v2/terminal/getPaperCount',//获取纸张数
}
