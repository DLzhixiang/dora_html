
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router'
import Header from '../Header'
import Back from '../Back'
import { version } from '../../../package.json'

const convertVersionToNumber = () => (
  version.split('.').reduce(function(sum, value) {
    return sum + Number(value);
  }, 0)
)

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      downloadVisible: false
    }
  }

  handlerVisible = (e) => {
    if(e.target.className == style['help-dora']){
      this.setState({
        downloadVisible: !this.state.downloadVisible
      })
    }else {
      this.setState({
        downloadVisible: false
      })
    }
  }

  render(){
    const { user } = this.props.duola;
    return(
      <div>
        <Header title="联系客服" hasBack={false}/>
        <div className={style['container']} onClick={this.handlerVisible}>
          <div className={style['download-box']}>
            <p className={style['first']}>微信扫码关注多拉打印，</p>
            <p>可以通过快速客服通道快速解决退款等问题～</p><br />
            <img src={require('./assets/app.jpg')} alt=""/>
            <p className={style.tel}>客服电话：400-160-3672</p>
            <div className={style['app-download']}>
              <img src="http://www.duoladayin.com:8070/image/help-dora.png" alt="" className={style['help-dora']}/>
              <img src="http://www.duoladayin.com:8070/image/help-download.png" alt="" className={style['help-download']} style={{visibility: this.state.downloadVisible ? 'visible' : 'hidden'}}/>
            </div>
          </div>
          <span className={style['version']}>版本号: {this.props.duola.version}（{convertVersionToNumber()}）</span>
        </div>
        <Back />
      </div>
    )
 }
}
