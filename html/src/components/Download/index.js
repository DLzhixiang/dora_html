import React, { Component } from 'react'
import style from './style.scss'
import classnames from 'classnames'
import { Link, hashHistory } from 'react-router'

let counter = 0;
let goOperation = () => {
  if(counter < 7){
    counter++
  }else {
    hashHistory.push('operation');
    counter = 0;
  }
}

const download = ({ small=false }) => (
  <div className={classnames(small?style['small']:'',style['app-download'])} onClick={goOperation}>
    <img src={require('./assets/app.png')} />
    <p>
    立即扫码下载多拉<br />
    体验优质打印服务
    </p>
  </div>
)

export default download
