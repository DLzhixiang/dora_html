
import React from 'react'
import style from './style.scss'
import classnames from 'classnames'
import {  Radio, Icon } from 'antd';
import { inject, observer } from 'mobx-react'

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px',
};

const displayMode = inject('duola')(observer(({duola}) => {
  const { showType, sortType } = duola;
  return (<div className={style['container']}>
              <div className={style['radio-group']}>
                  <label >
                      <span className={showType == 'row' ? style['row'] : style['row-o']}>
                        <input type="radio" name="sort" value="row" onChange={(e)=>duola.changeShowType(e.target.value)}/>
                      </span>
                  </label>
                  <label >
                      <span className={showType == 'column' ? style['column'] : style['column-o']}>
                        <input type="radio" name="sort" value="column" onChange={(e)=>duola.changeShowType(e.target.value)}/>
                      </span>
                  </label>
              </div>
              <div className={style['radio-group']}>
                  <label >
                      <span className={sortType == 'time' ? style['time'] : style['time-o']}>
                        <input type="radio" name="sort" value="time" onChange={(e)=>duola.changeSortType(e.target.value)}/>
                      </span>
                  </label>
                  <label >
                      <span className={sortType == 'case' ? style['case'] : style['case-o']}>
                        <input type="radio" name="sort" value="case" onChange={(e)=>duola.changeSortType(e.target.value)}/>
                      </span>
                  </label>
              </div>
           </div>)
}))

export default displayMode
