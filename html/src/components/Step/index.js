import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'

@inject('duola') @observer
export default class Step extends Component {
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className="container">
        <div className={style['box']} style={this.props.style}>
          <div className={style['stepLayout']}>
          {
            this.props.steps.map((item, index)=>{
                return <StepItem key={"step"+index} index={index} step={item} current={this.props.step == index}/>
            })
          }
          </div>
          <div className={style['line']}> </div>
        </div>
      </div>
    )
 }
}

class StepItem extends Component {
  constructor() {
    super()
  }
  render(){
      return (
        <div className={style['stepInfo']}>
            <a className={this.props.current ? style['current'] : style['stepSequence']}>{this.props.step}</a>
        </div>
      )
  }
}
