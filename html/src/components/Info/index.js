import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import $ from 'jquery'



@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
  }

  render(){
    const { user, version } = this.props.duola;
    return(
      <div className={style['container']}>
        <p className={style['location']}>{user.namecode}</p>
      </div>
    )
 }
}
