
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'

const goBack = (home,ev) => {
  if(home){
    hashHistory.push('')
  }else {
    hashHistory.goBack()
  }
}

const back = ({customClick, home=false, ev}) => {
  return <img src={home ? require('./assets/home.png') : require('./assets/back.png')} onClick={customClick || (()=>goBack(home, ev))} className={style['back']}/>
}

export default back
