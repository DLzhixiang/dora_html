
import React, { Component } from 'react'
import style from './style.scss'
import Header from '../Header'
import Download from '../Download'
import Back from '../Back'


export default class  extends Component {
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div>
        <Header title="如何打印" hasBack={false}/>
        <div className={style['content']}>
           <ul className={style['screenshot-list']}>
             <li>
               <img className={style['screenshot']} src={require('./assets/step0.png')}/>
               <div className={style['describe-box']}>
                 <span className={style['step']}>1 </span>
                 <p>
                   扫描二维码，前往应用商店下载多拉打印App, 立即体验便捷、方便的现代打印体验
                 </p>
               </div>
             </li>
              <li>
                <img className={style['screenshot']} src={require('./assets/step1.png')}/>
                <div className={style['describe-box']}>
                  <span className={style['step']}>2 </span>
                  <p>
                    从第三方App（例如“微信”或者“邮件”）中打开需打印的文件，点击右上角，选择“用其他应用打开”
                  </p>
                </div>
              </li>
              <li>
                <img className={style['screenshot']} src={require('./assets/step2.png')} />
                <div className={style['describe-box']}>
                  <span className={style['step']}>3</span>
                  <p>
                    选择“多拉打印”
                  </p>
                </div>
              </li>
              <li>
                <img className={style['screenshot']} src={require('./assets/step3.png')}/>
                <div className={style['describe-box']}>
                  <span className={style['step']}>4</span>
                  <p>
                    在 多拉打印App 中完成打印设置，并完成支付
                  </p>
                </div>
              </li>
              <li>
                <img className={style['screenshot']} src={require('./assets/step4.png')}/>
                <div className={style['describe-box']}>
                  <span className={style['step']}>5</span>
                  <p>
                   在任意多拉自助打印机上扫描屏幕二维码，选取订单进行打印
                  </p>
                </div>
              </li>
           </ul>
        </div>
        <Back />
      </div>
    )
 }
}
