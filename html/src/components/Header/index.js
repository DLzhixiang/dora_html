
import React, { Component } from 'react'
import classnames from 'classnames'
import style from './style.scss'
import { Link, hashHistory } from 'react-router'
import { inject, observer } from 'mobx-react'

function goHome(title){
  hashHistory.push('')
}

const Header = inject('duola')(observer(({duola, hasBack = true, title = '', hasShadow = true, hasHelp = true}) => {
  return (<header className={style['header']} style={hasShadow ? null : {boxShadow: 'none'}}>
      <div className={style['left-box']} onClick={hasBack && (()=>goHome(title))}>
        <img className={style.logo} src={require('./assets/logo.png')} alt=""/>
        <span className={style.title} >多拉打印</span>
        <img src={require('./assets/line.png')} alt="" className={title?'':'hide'}/>
        <span className={style['sub-title']}>{title}</span>
      </div>
      {
        hasHelp ? (
          <div className={classnames(style['right-box'])}>
            <div style={{float: 'left'}} onClick={()=>{
              duola.onChangeHelpTab('howuse')
              hashHistory.push('/help')
            }}>
              <img src={require('./assets/howuse.png')} className={style['bob']} alt=""/>
              <p> 如何打印 </p>
            </div>
            <div onClick={()=>{
              duola.onChangeHelpTab(' customercare')
              hashHistory.push('/help')
            }}>
              <img src={require('./assets/help.png')} alt=""/>
              <p> 联系客服 </p>
            </div>
          </div>
        ) : null
      }
    </header>)
}))

export default Header
