
import React, { Component } from 'react'
import { Row, Col } from 'antd';
import classnames from 'classnames'
import style from './style.scss'
import { bytesToSize, clipPath } from '../../utils'
import { inject, observer } from 'mobx-react'
import { imgType } from '../../utils/config'

function goHome(){
  hashHistory.push('/')
}

function goBack(){
  hashHistory.goBack()
}

const FileInfo = inject('duola')(observer(({duola, onClick, type, info}) => {
  return info ?   <div className={type == "setting" ? style['container-setting'] : style['container']}>
        <img className={style['type-img']} src={require(`./assets/file_${info.name.split('.').pop().toLowerCase()}.png`)}/>
        <p className={style['name']}>{clipPath(info.name,15)}</p>
        <table>
          <tbody>
            <tr>
              <td>创建时间：</td>
              <td>{info.creation_time.slice(0,10)}</td>
            </tr>
            <tr>
              <td>修改时间：</td>
              <td>{info.creation_time.slice(0,10)}</td>
            </tr>
            <tr>
              <td>文件大小：</td>
              <td>{bytesToSize(info.size)}</td>
            </tr>
            <tr className={style.pages}>
              <td>文件页数：</td>
              <td>{duola.fileInfo.pages ? duola.fileInfo.pages : <span className={style.loading}>加载中</span>}</td>
            </tr>
          </tbody>
        </table>
        <p className={style['imgage-text']} style={{display: imgType.includes(info.name.split('.').pop()) ? '' : 'none'}}>
          暂不支持U盘照片打印<br />
          该文件将以A4形式打印
        </p>
        {
          <button onClick={onClick}>
              打印该文件
          </button>
        }
    </div> : null
}))

export default FileInfo
