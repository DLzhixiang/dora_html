import { observable, action } from 'mobx'
import { apis } from '../apis'
import $ from 'jquery'

//建立socket连接
var io=require('socket.io-client');
var socket = io(apis.SOCKET_URL);
let hasSendId = false;

class SocketStore {
  @observable blackWhiteDiscount
  @observable blackWhitePrice
  @observable colorPringDiscount
  @observable colorPringPrice
  @observable code
  @observable canUse
  @observable hasColor
  @observable needReload

  constructor(rootStore) {
    this.rootStore = rootStore
    this.code = null
    this.blackWhiteDiscount = 1;
    this.blackWhitePrice = 0.1;
    this.colorPringDiscount = 1;
    this.colorPringPrice = 1;
    this.hasColor = false;
    this.canUse = [];
    this.setUpSocket();
    this.initSocketListener();
    this.needReload = false
  }

  @action setUpSocket = () => {
    socket.on('connect', ()=>{
      this.setSocketId();
      console.log('socket connect');
    })
    //服务器关闭
    socket.on('disconnect', () => {
      console.log('socket disconnect');
    });
    //重新启动服务器
    socket.on('reconnect', () => {
      console.log('socket reconnect');
    });

    socket.on('error', (error) => {
      console.log(error);
    });
  }

  @action initSocketListener() {
    //获取扫描二维码
    this.socketReceive('setCode2Terminal',({code})=>{
      this.setCode(code)
      this.socketSend('changeCode2Terminal')
    })
    //获取终端分组
    this.socketReceive('setTermGroup',(res)=>{
      // res && this.setPrice(res);
    })
    //获取更新指令
    this.socketReceive('reloadCommand',(res)=>{
      if(res){
        this.needReload = true;
      }
    })
  }

  //根据终端分组设置价格
  @action setPrice = ({blackWhiteDiscount, blackWhitePrice, colorPringDiscount, colorPringPrice, canUse, hasColor}) => {
      this.blackWhiteDiscount = blackWhiteDiscount;
      this.blackWhitePrice = blackWhitePrice;
      this.colorPringDiscount = colorPringDiscount;
      this.colorPringPrice = colorPringPrice;
      this.canUse = canUse;
      this.hasColor = hasColor;
  }

  //获取u盘打印价格
  getTermGroup = async () => {
    let res = await $.get(`${apis.SOCKET_URL}${apis.getTerminalGroup}?terminal_id=${this.rootStore.user.id}`);
    res && this.setPrice(res);
  }

  @action setSocketId = (userId) => {
    if(this.rootStore.user.id && !hasSendId){
      this.socketSend('socketId', this.rootStore.user.id);
      hasSendId = true
    }else if (userId && !hasSendId) {
      this.socketSend('socketId', userId);
      hasSendId = true
    }
  }

  //socket上传
  @action socketSend(type, param)
  {
      socket.emit(type, param)
  }

  //socket接收
  @action socketReceive(type, cb)
  {
      socket.on(type, cb)
  }

  //设置扫描二维码
  @action setCode(code){
      this.code = code;
  }

  //获取扫码号
  async fetchCode(){
     this.socketSend('getCode2Terminal',this.rootStore.user.id);
  }
}


export default SocketStore;
