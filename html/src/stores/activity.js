import { observable, action, computed, toJS } from 'mobx'
import { apis } from '../apis'
import $ from 'jquery'
import { version } from '../../package.json'

class Activity {
  constructor(rootStore) {
    this.rootStore = rootStore;
    this.feVersion = version;
    this.getTheme();
  }
  //获取主题设置
  @action async getTheme(){
      const { code, theme} = await $.post(`${apis.API_URL}${apis.getThemeUsed}`,{id: this.rootStore.user.id});
      if(code == 0){
        this.setTheme(theme);
      }
  }
  //设置屏保
  @action setTheme({saleprice,hasActivity,screensaver,activityPic}){
      this.screenSaver = screensaver;
      this.activityPic = activityPic;
      this.salePrice = saleprice;
      this.hasActivity = hasActivity;
  }
}

export default Activity
