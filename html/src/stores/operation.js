import { observable, action } from 'mobx'
import { trim } from '../utils'
import { imgType, pdfType, excelType, wordType } from '../utils/config'
import { data, map } from '../containers/FeedBack/data'


class Operation {
  @observable needReload
  constructor(rootStore) {
    this.rootStore = rootStore;
    this.orderInfo = {};
  }

  formatOrderInfo = (orders) => {
    let maxOrder = 0;
    let maxPrice = orders[maxOrder].realPrice;
    orders.map((order,index)=>{
      if(order.realPrice > maxPrice){
          maxOrder = index
          maxPrice = order.realPrice
      }
    })
    let isWord = 0, isPhoto = 0, isUdiskPhoto = 0;
    // if(orders[maxOrder].platform == undefined &&  imgType.includes(orders[maxOrder].fileName.split('.').pop())){
    //   isImage = 1;
    // }
    if(wordType.includes(orders[maxOrder].fileName.trim().split('.').pop())){
      isWord = 1;
    }
    if(excelType.includes(orders[maxOrder].fileName.trim().split('.').pop())){
      isWord = 1;
    }
    if(orders[maxOrder].imgUrl && orders[maxOrder].imgUrl.length){
      isPhoto = orders[maxOrder].platform == 4 ? 1 : 2;
    }
    if(orders[maxOrder].platform == undefined &&  imgType.includes(orders[maxOrder].fileName.trim().split('.').pop())){
      isUdiskPhoto = 1;
    }

    const  { platform, type, pagesPerSheet, printType} = orders[maxOrder];
    const orderInfo = {
      platform: platform || 6,
      type,
      pagesPerSheet,
      printType,
      isWord,
      isPhoto,
      isUdiskPhoto
    };
    let res = Object.keys(orderInfo).map((key)=>data[key][orderInfo[key]]);

    let tags = res.reduce((sum,cur)=>(
      map[cur] ? sum.concat(map[cur]) : sum
    ),[])
    this.setOrderInfo(orderInfo)
  }

  setOrderInfo = (info) => {
      this.orderInfo = info;
  }

}


export default Operation;
