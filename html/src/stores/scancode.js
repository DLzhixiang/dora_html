import { observable, action, computed, toJS } from 'mobx'
import { hashHistory } from 'react-router'
import { Modal, Button } from 'antd';
import { apis } from '../apis'
import $ from 'jquery'
import store from 'store'

import { order } from './mock'

let timer = null, percent_interval = null, empty_timeout = null;

class ScanCode {
  @observable isPrint
  @observable code
  @observable showSendPrintingStatus
  constructor(rootStore) {
    this.rootStore = rootStore
    this.shouldDownload = true
    this.shouldPrint = true
    this.fininshedTask = 0
    this.shoulfGetFile = true
    this.taskNumber = 0
    this.taskList = []
    this.downloadIndex = 0;
    this.printIndex = 0;
    this.code = null;
    this.showSendPrintingStatus = false;
    this.fetchScanCounter()
  }

  async obtainTerminalStatus(){
    let result = await $.get(`${apis.API_URL}${apis.TERMINAL_PATH}${this.rootStore.user.id}/${apis.obtainStatusByPhone}`);
    return result
  }

  resetTerminalStatus(){
    $.post(`${apis.API_URL}${apis.TERMINAL_PATH}${this.rootStore.user.id}/${apis.obtainStatusByPhone}`);
  }

  fetchScanCounter = () => {
    let counter =  store.get('scanCounter')
    $.get(`${apis.SOCKET_URL}sendCodeError`,counter);
  }

  resetPrintStatus = () => {
    this.shouldDownload = true
    this.shouldPrint = true
    this.fininshedTask = 0
    this.shoulfGetFile = true
    this.taskNumber = 0
    this.taskList = []
    this.downloadIndex = 0;
    this.printIndex = 0;
    this.rootStore.setPrintPercent(0.1)
    this.showSendPrintingStatus = false;
    this.resetTerminalStatus();
  }

  fetchCode = async () => {
    var counter =  store.get('scanCounter') || { done: 0, fail: 0, terminalId: this.rootStore.user.id, device: 'brother'};
    let code = await $.ajax(`${apis.SOCKET_URL}${apis.getCode}?terminal_id=${this.rootStore.user.id}`).done(function() {
           counter.done+=1;
           store.set('scanCounter',counter)
        })
        .fail(function(XMLHttpRequest, textStatus, errorThrown) {
          counter.fail+=1;
          store.set('scanCounter',counter)
        })
    this.setCode(code)
  }

  //设置扫描二维码
  @action setCode(code){
      this.code = code;
  }

  checkLoginByPhone(){
    this.obtainTerminalStatus().then(({
      data: {
        status
      }
    })=>{
      if(status == 'empty'){
        let timeout = setTimeout(() => {
                        modal.destroy()
                        hashHistory.push('/')
                      },5000);
        const modal = Modal.success({
          content: '您还没有打印列表 请先至App下单！',
          width: 600,
          iconType: null,
          onOk: ()=>{
            modal.destroy()
            hashHistory.push('/')
            clearTimeout(timeout)
          }
        });
      }
      if(status == 'scan'){
        hashHistory.push('waiting')
      }
    })
  }

  setshouldPrintStatus = (value) => {
    this.shouldPrint = value
  }

  setshouldDownloadStatus = (value) => {
    this.shouldDownload = value
  }

  resetDownloadStatus = () => {
    this.setshouldDownloadStatus(true);
  }

  handlerIncPrintPercent = (current, target) => {
    let speed = (target - current) / 32;
    this.rootStore.setPrintPercent(current + speed)
  }

  fetchThePrintFile = () => {
    this.obtainTerminalStatus().then(({
      data: {
        status,orders,message
      }
    })=>{
      if(status == 'cancel'){
        this.rootStore.setErrorModal(true,'您已在手机端取消本次打印')//打印失败！
      }
      if(status == 'overstep'){
        this.rootStore.setErrorModal(true,message)//超出打印范围！
        this.resetTerminalStatus();
      }
      if(status == 'print' && this.shoulfGetFile){
        this.rootStore.operation.formatOrderInfo(orders);
        this.shoulfGetFile = false
        hashHistory.push('/print')
          this.taskNumber = orders.length;
          let offset = 1 / this.taskNumber / 10;
          let target = null;
          percent_interval = setInterval(()=>{
            target = this.printIndex/this.taskNumber - offset;
            if(this.printIndex){
               if(this.rootStore.printPercent < target){
                 this.handlerIncPrintPercent(this.rootStore.printPercent, target)
               }
            }
          },2000)
        orders.map(({ num: copies, startPage, endPage, printType, pagesPerSheet, id, dataUrl, type, imgUrl, color } ,index)=>{
          let duplex;
          if( typeof printType == "boolean"){
            duplex = Number(printType) + 1
          }else {
            duplex = printType
          }
          const data = {
            copies,
            duplex,
            from:startPage + 1,
            to: endPage + 1,
            pages_per_sheet: pagesPerSheet,
            color: color
          }
          this.taskList[index] = {
            data,
            dataUrl,
            id,
            imgUrl,
            type
          };
        })
        timer = setInterval(()=>{
          if(this.taskNumber == this.fininshedTask){
              this.taskNumber && this.rootStore.setPrintPercent(1);
              clearInterval(timer)
              clearInterval(percent_interval)
          }else {
            if(this.shouldDownload){
              this.setshouldDownloadStatus(false);
              this.downloadIndex && (this.taskList[this.downloadIndex - 1].data.file = this.rootStore.file);
              (this.downloadIndex < this.taskNumber) && this.downloadTask(this.taskList[this.downloadIndex++]);
            }

            if(this.shouldPrint && this.taskList[this.printIndex].data.file){
              this.rootStore.setOrderId(this.taskList[this.printIndex].id)
              this.setshouldPrintStatus(false);
              (this.printIndex < this.taskNumber)  && this.printTask(this.taskList[this.printIndex++])
            }

            if(this.printIndex == this.taskNumber){
              this.showSendPrintingStatus = true;
            }
          }
        })
      }
    })
  }

  downloadTask = ({dataUrl, imgUrl, type}) => {
    type == 4 ? this.rootStore.onSendMessage('MULTIDOWNLOAD','', imgUrl) : this.rootStore.onSendMessage('FILEDOWNLOAD','', dataUrl)
  }


  printTask = ({data, type}) => {
    type == 4 ? this.rootStore.onSendMessage('PHOTOPRINTING','',data.file) : this.rootStore.onSendMessage('PRINTERPRINTING','',[data])
  }

  fininshTask = () => {
    this.fininshedTask = this.fininshedTask + 1
    this.rootStore.setPrintPercent(this.fininshedTask / this.taskNumber)
  }
}

export default ScanCode
