import { observable, action } from 'mobx'
import idb from 'idb'
import { hashHistory } from 'react-router'

class LocalLog {
  @observable needReload
  constructor(rootStore) {
    this.rootStore = rootStore
    this.dataBase = null
    this.dbStore = 'logStore'
    this.dbTransaction = 'logts'
    this.setUpLog();
  }

  @action setUpLog = () => {
      this.dataBase = idb.open(this.dbStore, 1, upgradeDB => {
        upgradeDB.createObjectStore(this.dbTransaction);
      })
    setTimeout(() => {
        this.count().then((num)=>{
            if(num > 50000){
                this.clear();
            }else{
                console.log(`当前数据量${num}`)
            }
        })
    }, 3000);                                          

      window.onerror = function(errorMessage) {
        console.log(errorMessage, 'error');
     }
    
     this.rewriteConLog()

  }

  @action get = (key) => {
    return this.dataBase.then(db => {
      return db.transaction(this.dbTransaction)
        .objectStore(this.dbTransaction).get(key);
    });
  }

  @action set = (key,val) => {
    return this.dataBase.then(db => {
        const tx = db.transaction(this.dbTransaction, 'readwrite');
        tx.objectStore(this.dbTransaction).put(val, key);
        return tx.complete;
      });
  }
  
  @action delete = (key) => {
    return this.dataBase.then(db => {
        const tx = db.transaction(this.dbTransaction, 'readwrite');
        tx.objectStore(this.dbTransaction).delete(key);
        return tx.complete;
      });
  }

  @action clear = (key) => {
    return this.dataBase.then(db => {
        const tx = db.transaction(this.dbTransaction, 'readwrite');
        tx.objectStore(this.dbTransaction).clear();
        return tx.complete;
      });
  }

  @action getAll = () => {
    return this.dataBase.then(db => {
        return db.transaction(this.dbTransaction)
          .objectStore(this.dbTransaction).getAll();
      })
  }

  @action count = () => {
    return this.dataBase.then(db => {
        return db.transaction(this.dbTransaction)
          .objectStore(this.dbTransaction).count()
      })
  }

  //重写console.log 
  @action rewriteConLog = () => {
    let nativeLog = console.log;
    console.log = (value,type = 'normal') => {
        nativeLog(value);
        if(value == 'log'){
            hashHistory.push('/loglist')
        }
        this.set(Date.now(), {data: JSON.stringify(value), time: new Date().toLocaleString(),type, key: Date.now()});
    }
  }
}


export default LocalLog;
