
import { observable, action, computed, toJS } from 'mobx'
import store from 'store'
import ScanCode from './scancode'
import Socket from './socket'
import Log from './log'
import Operation from './operation'
import Activity from './activity'
import $ from 'jquery'
import { hashHistory } from 'react-router'
import moment from 'moment'
import { apis } from '../apis'
import { isNumer } from '../utils'
import { terminalOnFetchRate } from '../utils/config'

//准备阶段
const onReady = () => new Promise((resolve, reject) => document.addEventListener('liasicaectron-ready', () => resolve() ))
//接收消息
const onReceiveMessage = () => new Promise( (resolve, reject) => liasicaectron.listen(msg => resolve(JSON.parse(msg)) ));
//检测状态码
// const hitCode = (code, target) => (target.some((item)=>code.indexOf(Number(item)) > -1));

// const asd = "o1={)+I=!+'d%0]jjdre@n<wIalztE$%bU}Mt-Y0f_yC%}H~ow5E]|qkp+.LH{YP"
//广告代码
let ad_interval = null, sleep = true, fetchUpIndex = -1, isError = false;

let status_interval = null, percent_interval = null, startTime = '', endTime = '', hasSendId=false;
class Duola {
  @observable isReady
  @observable isPrinterReady
  @observable isBarcodeReady
  @observable printPercent
  @observable orderId
  @observable printer_id
  @observable user
  @observable loginVisible
  @observable showError
  @observable errorMessage
  @observable showType
  @observable sortType
  @observable usbDriveData
  @observable copyStatus
  @observable fileInfo
  @observable helpTab
  @observable file
  @observable version

  constructor() {
    this.isReady = false
    this.isPrinterReady = false
    this.isBarcodeReady = false
    this.printPercent = 0.1
    this.orderId = ''
    this.printer_id = ''
    this.user = store.get('user') || {}
    this.loginVisible = false
    this.showError = false
    this.errorMessage = ''
    this.showType = 'row'
    this.sortType = 'time'
    this.usbDriveData = []
    this.USBStatus = 0
    this.copyStatus = false
    this.fileInfo = {}
    this.helpTab = 'howuse'
    this.file = null
    this.version = '1.5.2'
    this.scanCode = new ScanCode(this)
    this.socket = new Socket(this)
    this.log = new Log(this)
    this.operation = new Operation(this)
    this.activity = new Activity(this)
    this.shouldErrTimeout = false;
  }

  setAdSleep(){
    ad_interval = setInterval(()=>{
      console.log('长时间没用啦')
      //发送闲置命令
      this.onSendMessage('TERMINALIDLE','idle');
      sleep = true;
    },1000*60*3)
  }

  onWakeUp(){
    console.log('被用啦')
    if(sleep) this.onSendMessage('TERMINALIDLE','');
    sleep = false;
    ad_interval && clearInterval(ad_interval);
    this.setAdSleep()
  }

  onBodyWake(){
    this.setAdSleep()
    $('body').on('click',()=>{
      this.onWakeUp()
    })
  }

  async fetchOnReady(){
    if(!this.isReady){
      const rlt = await onReady();
      this.setReady()
      this.onFetchState()// 定时获取状态
      this.onListenMessage()
      this.onBodyWake()
      this.checkErrorBefore()
    }
  }

  @action setReady(){
    this.isReady = true
  }

  @action checkErrorBefore(){
    if(store.get('printPercent')){
      this.fetchUpPrintingError({
        time: new Date(),
        orderId: 0,
        terminal: this.user.namecode,
        errorCode: 0,
        errorDesc: `打印中途重启${store.get('printPercent')}`,
        file: null
      },'printingError')
      store.set('printPercent', null)
    }
  }

  @action setPrinterReady(state = true){
    this.isPrinterReady = state
  }

  @action setBarcodeReady(state = true){
    this.isBarcodeReady = state
  }

  @action setUSBDetector(value){
    this.usbDriveData = value.detail
    this.USBStatus = value.got
  }

  @action setCopyStatus(value){
    this.copyStatus = value
  }

  @action setFileInfo({file_path, file_id, pages}){
    this.fileInfo = {file_path, file_id, pages}
  }

  @action USBEject(){
    this.fileInfo = {}
    this.USBStatus = 0
    this.usbDriveData = []
    this.copyStatus = false
    this.showType = 'row'
    this.sortType = 'time'
  }

  @action setFileUrl(file){
    this.file = file
  }

  @action setVersion(value){
    this.version = value
  }

  onFetchState(){
    this.onSendMessage('PRINTERSTATUS');
    status_interval = setInterval(()=>{
      this.onSendMessage('PRINTERSTATUS');
      this.onSendMessage('VERSION');
      this.onSendMessage('TERMINALINFO','',this.user);
      this.fetchUserInfo()
      this.fetchPrinter()
    },60 * 1000)
  }

  incPrintPercent = () => {
      let speed = null;
      percent_interval = setInterval(()=>{
        speed = (0.9 - this.printPercent) / 16;
        this.setPrintPercent(this.printPercent + speed)
      }, 1000);
  }

  onSendMessage(action, detail = '', print_data = []){
    const data = { action, detail }
    if(action == 'FILEPAGES'){
      data.disk_file = print_data
    }else if(action == 'DISKPRINTING'){
      data.disk_file = print_data
      delete data.detail
    }else if (action == 'FILEDOWNLOAD') {
      data.url = print_data;
    }else if (action == 'MULTIDOWNLOAD') {
      data.urls = print_data;
    }else if(action == 'PHOTOPRINTING' || action == 'PRINTERPRINTING'){
      data.disk_files = print_data
      delete data.detail
    }else if (action == 'TERMINALINFO' ) {
      data.info = print_data
    }
    console.log(JSON.stringify(data))
    liasicaectron.send(JSON.stringify(data));
  }

  onReselect(){
    this.copyStatus = false
    this.setFileInfo({})
  }

  @action clearUserInfo(){
    store.set('user',{})
    this.user = {}
    this.loginVisible = true
  }


  async onListenMessage(){
    while(true){
      const msg =  await onReceiveMessage()
      console.log(msg)
      switch (msg.action) {
        case 'NOTIFY':
          if(msg.detail != 'exist' && msg.device == 'printer') this.socket.socketSend('printerUnknown')
          break;
        case 'PRINTERSTATUS':
          // ++fetchUpIndex;
          this.setPrinterReady(msg.normal)
          // if(!msg.code) break;
          // if((fetchUpIndex % 5) == 0 && (fetchUpIndex % 30) != 0 && (fetchUpIndex % 60) != 0){
          //   this.fetchUpDeviceStatusForOperation(msg, "fiveMinutes")
          // }else if ((fetchUpIndex % 30) == 0 && (fetchUpIndex % 60) != 0) {
          //   hitCode(terminalOnFetchRate.thirty,msg.code) && this.fetchUpDeviceStatusForOperation(msg, "thirtyMinutes")
          // }else if ((fetchUpIndex % 60) == 0) {
          //   hitCode(terminalOnFetchRate.sixty,msg.code) && this.fetchUpDeviceStatusForOperation(msg, "sixtyMinutes")
          // }
          //需要 轮询上报信息
          // if(msg.code.indexOf('40000')<0 && msg.code.indexOf('100003')<0) this.fetchUpDeviceStatus(msg);
          // if(!msg.normal){
          //   hashHistory.push({ pathname:'/error', state: msg.code });
          //   isError = true
          // }else if(isError && msg.normal){
          //   hashHistory.push('');
          //   isError = false
          // }
          break;
        case 'VERSION':
          this.setVersion(msg.version)//唤醒
          break;
        case 'LOGIN':
          this.clearUserInfo()//唤醒
          break;
        case 'BARCODESTATUS':
          this.setBarcodeReady(msg.detail == 'normal')
          //需要 扫描头信息上报
          this.fetchUpScanStatus(msg.detail == 'normal'?0:1)
          break;
        case 'PRINTINGSTATUS':
          this.onWakeUp()//唤醒
          this.scanCode.showSendPrintingStatus &&  this.fetchUpPrintingStatusForOperation(msg.detail, 'printingStatus')
          switch (msg.detail) {
            case 'printing':
              startTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
              break;
            case 'success':
              endTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
              this.scanCode.taskNumber != 0 && this.scanCode.fininshTask()
              this.scanCode.taskNumber == 0 && this.setPrintPercent(1)
              this.scanCode.setshouldPrintStatus(true)
              //需要 标记打印任务 操作
              percent_interval && clearInterval(percent_interval)
              this.fetchUpPrintStatus(3) //正式发布需要去掉注释
              break;
            case 'error':
              hashHistory.push({ pathname:'/error', state: Object.keys(JSON.parse(msg.err)) });
              this.setPrintPercent(0.1);
              percent_interval && clearInterval(percent_interval)
               //需要 标记打印任务 操作
              this.fetchUpPrintStatus(2) //正式发布需要去掉注释
              this.fetchUpPrintingError({
                time: new Date(),
                orderId: this.orderId,
                terminal: this.user.namecode,
                errorCode: msg.errcode,
                errorDesc: msg.error_desc,
                file: msg.file
              },'printingError')
              break;
            case 'failed':
              //toast error message
              percent_interval && clearInterval(percent_interval)
              this.setErrorModal(true,'打印失败！')//打印失败！
              this.errTimeout()
              this.fetchUpPrintingError({
                time: new Date(),
                orderId: this.orderId,
                terminal: this.user.namecode,
                errorCode: msg.errcode,
                errorDesc: msg.error_desc,
                file: msg.file
              },'printingError')
              break;
            default:
              break;
          }
          break;
          case 'FILEDOWNLOAD':
            this.onWakeUp()//唤醒
            switch (msg.detail) {
              case 'downloading':
                if((msg.errcode != 0)){
                  this.setErrorModal(true,`网络太卡啦！多拉不能下载啦！${msg.errcode}`)//打印失败！
                  this.errTimeout()
                }
                break;
              case 'success':
                await this.setFileUrl(msg.file)
                this.scanCode.setshouldDownloadStatus(true)
                break;
              case 'error':
                this.setErrorModal(true,`网络太卡啦！多拉不能下载啦！${msg.errcode}`)
                this.errTimeout()
                break;
              default:
                break;
            }
          break;
          case 'USBDETECTOR':
            switch (msg.got) {
                case 1:
                    this.setUSBDetector(msg)
                    break;
                case 0:
                    this.setUSBDetector(msg)
                    break;
                case -1:
                    this.setUSBDetector(msg)
                    break;
                default:
                    break;
            }
          break;
          case 'FILECOPIED':
            switch (msg.copied) {
                case false:
                    this.setCopyStatus(msg.copied)
                    this.setErrorModal(true, msg.detail)
                    break;
                case true:
                    this.setCopyStatus(msg.copied)
                    break;
                default:
                    break;
            }
          break;
          case 'FILEPAGES':
            switch (msg.detail) {
                case 'success':
                    this.setFileInfo(msg)
                    break;
                case 'failed':
                    this.setErrorModal(true, msg.err)
                    this.setFileInfo({})
                    break;
                default:
                    break;
            }
          break;
          case 'USBEJECT':
            switch (msg.detail) {
              case true:
                switch (msg.DISKPRINTING) {
                  case false:
                    this.USBEject()
                    hashHistory.push('')
                    break;
                  case true:
                    this.incPrintPercent()
                    this.USBEject()
                    break;
                  default:
                    break;
                }
              case false:
                  break;
              default:
                  break;
            }
          break;
          case 'MULTIDOWNLOAD':
            switch (msg.detail) {
              case 'success':
                  const files =  msg.files.map((file)=> ({file: file.file}));
                  await this.setFileUrl(files)
                  this.scanCode.setshouldDownloadStatus(true)
                  break;
              case 'error':
                  this.setErrorModal(true,`网络太卡！多拉不能下载啦！`)
                  this.errTimeout()
                  break;
              default:
                  break;
            }
          break;
        default:
          break;
      }
    }
  }

  //获取订单信息
  async fetchVerifycode(id){
    if(!isNumer(id)) return this.setErrorModal(true,'打印码无效！');
    if(this.user.id == undefined) return;
    let data = await $.get(`${apis.API_URL}${apis.verifycode}/${id}/id/${this.user.id}`)
    if(data.code!='0'){
      this.setErrorModal(true,data.message)
      return ;
    }
    this.setOrderId(data.data.id)
    this.onSendMessage('FILEDOWNLOAD','',data);
    hashHistory.push({ pathname:'/preview', state: data.data })
  }

  errTimeout = () => {
    setTimeout(()=>{
      this.setErrorModal(false)
      hashHistory.push('');
    },1000 * 5)
  }

  @action setOrderId(id){
    this.orderId = id
  }

  onPrint(data){
    //开始打印
    this.onSendMessage('PRINTERPRINTING','',data);
  }

  setPrintPercent(value){
    this.printPercent = value
  }

  changeShowType(value){
    this.showType = value
  }

  changeSortType(value){
    this.sortType = value
  }

  onChangeHelpTab(value){
    this.helpTab = value
  }

  //上传打印状态 printstatus 打印状态 0：未开始； 1：进行中 ；2：出故障; 3:已完成
  async fetchUpPrintStatus(status){
    const parmas = {
      orderId: this.orderId,
      printerId: this.printer_id,
      terminalId: this.user.id,
      status
    }
    if(status == 3) {
      parmas.startTime = startTime;
      parmas.endTime = endTime;
    }
    let data = await $.post(`${apis.API_URL}${apis.printstatus}`,parmas);
    if(data.code!='0'){
      return ;
    }
  }

  //上传打印机设备状态  devicestatus
  async fetchUpDeviceStatus({ normal, code, supplies }){
    if(this.user.id == undefined) return;
    if(!this.printer_id)return;
    const printer_err = [];
    printer_err.push({
      print_id: this.printer_id,
      device_status:{
        normal,
        code,
        supplies
      },
    })
    let data = await $.post(`${apis.API_URL}${apis.devicestatus}`,{
      terminal_id: this.user.id,
      printer_err: JSON.stringify(printer_err)
    })
    if(data.code!='0'){
      return ;
    }
  }

  //上传扫描仪设备状态  scanstatus
  async fetchUpScanStatus(scanStatus){
    if(this.user.id == undefined) return;
    let data = await $.post(`${apis.API_URL}${apis.scanstatus}`,{
      scanStatus,
      terminal_id: this.user.id
    })
    if(data.code!='0'){
      return ;
    }
  }

  //上传设备信息到运维服务器
  fetchUpDeviceStatusForOperation(msg, type){
    if(this.user.id == undefined) return;
    if(!this.printer_id)return;
    this.socket.socketSend(type,{
      terminal_id: this.user.id,
      printer_status: JSON.stringify(msg),
      printing_status: ' '
    })
  }

  //上传打印信息到运维服务器
  async fetchUpPrintingStatusForOperation(detail, type){
    if(this.user.id == undefined) return;
    this.socket.socketSend(type,{
      terminal_id: this.user.id,
      printer_status: ' ',
      printing_status: detail,
      order_id: this.orderId
    })
  }

  //上传打印失败信息到运维服务器
  fetchUpPrintingError(msg, type){
    this.socket.socketSend(type,msg)
  }


  @action setPrinterId(data){
    if(data[0]){
      this.printer_id = data[0].id
    }else{
      this.printer_id = ''
    }
  }

  shouldFetchLogin(){
    return this.user.id == undefined
  }

  //namecode = '多拉终端1号', secretcode = 'duola'
  async fetchLogin(namecode, secretcode){
    //首先需要判断是否为空值，空值则通知弹窗
    if(!this.shouldFetchLogin()) return;
    let data = await $.post(`${apis.API_URL}${apis.login}`,{
      namecode,
      secretcode
    })
    if(data.code!='0'){
      this.setErrorModal(true,data.message)
      return ;
    }
    this.setLoginVisible(false)
    this.setUser(data.data)
    this.fetchPrinter()
  }

  async fetchUserInfo(){
    if(this.user.id == undefined) return;
    let data = await $.get(`${apis.API_URL}${apis.userinfo}/${this.user.id}`)
    if(data.code!='0'){
      return ;
    }
    this.setUser(data.data)
  }

  @action setUser(data){
    this.user = data
    this.socket.setSocketId(data.id)
    store.set('user', data)
  }

  @action setLoginVisible(value){
    this.loginVisible = value
  }

  shouldFetchPrinter(){
    return this.user.id != undefined
  }

  async fetchPrinter(){
    if(!this.shouldFetchPrinter()) return;
    let data = await $.get(`${apis.API_URL}${apis.printers}?tid=${this.user.id}`)
    if(data.code!='0'){
      return [];
    }
    this.setPrinterId(data.data)
  }

  @action setErrorModal(visible, errmsg){
    this.showError = visible
    this.errorMessage = errmsg
  }


  //获取二维码信息
  async fetchOrder(params){
    let order = await $.post(`${apis.API_URL}${apis.submit}`, params)
    if(order.code =='0'){
      return order.data;
    }
  }

  async fetchPayStatus(id){
    let order = await $.get(`${apis.API_URL}${apis.orderStatus}${id}`);
    if(order.data.status == 2){
      return true;
    }
    return false;
  }
}


export default Duola;
