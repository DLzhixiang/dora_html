
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import Modal from 'react-modal'
import { trim } from '../../utils'
import Header from '../../components/Header'
import Back from '../../components/Back'

import Download from '../../components/Download'

let timeout  = null;

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
  }

  componentDidMount() {
    timeout = setTimeout(() => {
      hashHistory.push('/')
    }, 30000);
  }

  componentWillUnmount() {
    timeout && clearTimeout(timeout)
  }

  handlerFeedback = (value) =>{
    hashHistory.push({ pathname:'/feedback', state: value });
  }

  render(){
    return(
      <div className={style.container}>
        <Header title = '申请退款'/>
        <div className={style.content}>
          <div className={style.left}>
            <h3>
            <img src="http://www.duoladayin.com:8070/image/dora-refund.png"  className={style['dora-refund']}/>
            多拉很抱歉…</h3>
            <p>本次任务已取消<br />您可以前往其他多拉打印机重新打印该文件</p>
          </div>
          <div className={style.right}>
          </div>
        </div>
        <span className={style['help-box']}>
          多拉24小时客服电话：400-160-3672
          <img src={require('../Error/assets/help.png')} height="40" className={style.help}/>
        </span>
        <Back/>
      </div>
    )
  }
}
          // onRequestClose={()=>{this.props.user.setLoginVisible(false)}}
