
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link } from 'react-router'
import $ from 'jquery'
import CustomerCare from '../../components/CustomerCare'
import HowUse from '../../components/HowUse'
import {version} from '../../../package.json'

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
  }

  render(){
    const { helpTab } = this.props.duola;
    return(
      <div>
          {
            helpTab == 'howuse' ? < HowUse/> : < CustomerCare/>
          }
      </div>
    )
 }
}
