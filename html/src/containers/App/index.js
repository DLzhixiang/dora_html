
import React, { Component } from 'react'
import style from './style.scss'
import $ from 'jquery'
import { Link, hashHistory, browserHistory } from 'react-router'
import Helmet from 'react-helmet'
import Info from '../../components/Info'
import UrlMap from './UrlMap.json'

let timer,
    counter = 0,
    residenceTime = 0;

export default class App extends Component{
  constructor(props){
    super(props)
  }

  componentDidMount(){
    this.onWelcome();
    $('body').on('click',()=>{
      counter = 0;
    })
  }

  componentWillUnmount(){
    timer && clearInterval(timer);
  }

  componentWillReceiveProps(nextProps){
    if(this.props.location.pathname != nextProps.location.pathname){
      residenceTime = 0;
    }
  }

  render(){
    return (
      <div>
        <Helmet>
          <title>Dora</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        </Helmet>
        {this.props.children}
        <Info />
      </div>
    )
  }

  onWelcome(){
      timer = setInterval(()=>{
          if(counter >= 60){
            counter = 0;
            hashHistory.push('')
          }else if(browserHistory.getCurrentLocation().hash.indexOf("#/pay") > -1 || browserHistory.getCurrentLocation().hash.indexOf("#/print") > -1 || browserHistory.getCurrentLocation().hash == "#/" || browserHistory.getCurrentLocation().hash.indexOf("#/error") > -1 || browserHistory.getCurrentLocation().hash.indexOf("#/printerlist") > -1  ) {
            counter = 0;
          }else if(browserHistory.getCurrentLocation().hash.indexOf("#/waiting") > -1){
            counter = counter + 0.5;
          } else {
            counter++;
          }
          //记录停留时间
          residenceTime++;
      },1000)
  }
}
