
import React, { Component } from 'react'
import style from './style.scss'
import classnames from 'classnames'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import ProgressBar from 'progressbar.js'
import Header from '../../components/Header'
import Step from '../../components/Step'
import Modal from 'react-modal'

//打印成功！请取件！

let timeout = null;
const customStyles = {
  overlay: {
    zIndex: 10
  }
};

@inject('duola') @observer
export default class extends Component {

  constructor(props){
    super(props)
    this.bar = null;
  }

  componentWillmount(){
    this.props.duola.setPrintPercent(0.1);
  }

  componentDidMount() {
    this.bar = new ProgressBar.Line(this.progress,{
      strokeWidth: 2,
      easing: 'easeInOut',
      duration: 1400,
      color: '#111',
      trailColor: 'rgba(0,0,0,0)',
      trailWidth: 2,
      svgStyle: {width: '100%', height: '100%'},
      text: {
        style: {
          // Text color.
          // Default: same as stroke color (options.color)
          color: '#FEE800',
          width:'120px',
          textAlign: 'right',
          position: 'absolute',
          top: '0px',
          lineHeight: '44px',
          fontSize: '36px',
          padding: 0,
          margin: 0,
          transform: {
            prefix: true,
            value: 'translate(-110%, 30%)'
          }
        },
        autoStyleContainer: false
      },
      from: {color: '#FFEA82'},
      to: {color: '#ED6A5A'},
      step: (state, bar) => {
        bar.setText(Math.round(bar.value() * 100) + '%');
        bar.text.style.left = Math.round(bar.value() * 100) + '%';
      }
    });
    this.bar.animate(this.props.duola.printPercent);
    timeout = setTimeout(()=>{
      this.props.duola.socketSend("printTooLong")
      console.log('打印超时');
    }, 1000 * 120);//停留超过两分钟手动检查
  }

  componentDidUpdate(prevProps, prevState) {
    const { printPercent } =  this.props.duola;
    this.bar && this.bar.animate(printPercent,()=>{
      if(printPercent >= 1){
        hashHistory.push('/success')
      }
    });
  }

  componentWillUnmount() {
    this.bar.destroy();
    this.props.duola.setPrintPercent(0.1);
    clearTimeout(timeout);
    console.log('离开打印页');
  }



  onCloseModal = () => {
    this.props.duola.setErrorModal(false,'')
    hashHistory.replace('/')
  }

  render(){
    const { showError, errorMessage, printPercent } = this.props.duola;
    return(
      <div className={style.bird}>
        <Header hasBack={false} hasHelp={false} title="打印中"/>
        <div className={style.wrapper}>
          <div className={style.timer}>
               <span></span>
          </div>
          <div className={style.gameover}>GAME OVER</div>
          <h1>干死鸟 <span>(射击飞行中的小鸟)</span></h1>
          <h2>等待打印时活动一下!</h2>
          <input className={classnames(style['input-circle'],style['input-circle1'])} type="radio" id="circle1" />
         <input className={classnames(style['input-circle'],style['input-circle2'])} type="radio" id="circle2" />
         <input className={classnames(style['input-circle'],style['input-circle3'])} type="radio" id="circle3" />
         <input className={classnames(style['input-circle'],style['input-circle4'])} type="radio" id="circle4" />
         <input className={classnames(style['input-circle'],style['input-circle5'])} type="radio" id="circle5" />
         <input className={classnames(style['input-circle'],style['input-circle6'])} type="radio" id="circle6" />

          <label htmlFor="circle1" className={classnames(style['pajaro'],style['pajaro1'])}><span></span></label>
          <label htmlFor="circle2" className={classnames(style['pajaro'],style['pajaro2'])}><span></span></label>
          <label htmlFor="circle3" className={classnames(style['pajaro'],style['pajaro3'])}><span></span></label>
          <label htmlFor="circle4" className={classnames(style['pajaro'],style['pajaro4'])}><span></span></label>
          <label htmlFor="circle5" className={classnames(style['pajaro'],style['pajaro5'])}><span></span></label>
          <label htmlFor="circle6" className={classnames(style['pajaro'],style['pajaro6'])}><span></span></label>
          <div className={style.sum}>分数:</div>
          <footer><a>游戏由铮琦制作</a></footer>
        </div>
        <div className={style['container']}>
          <div className={style['progress-box']} ref={e => this.progress = e }>
            <div className={style['border-box']}></div>
          </div>
          {
            this.props.location.state == "usb" ?
              <p className={style['tips2']}>多拉小贴士：除了U盘打印，多拉还支持多拉APP取件哦～</p>
              : <p className={style['tips2']}>多拉小贴士：除了使用多拉APP取件，多拉还支持U盘打印哦～</p>
          }
        </div>
        {
          this.props.location.state == "usb" && <Step style={{marginTop: '15vw'}} steps={['插入U盘','选择文件','打印设置','支付','完成打印']} step={4}/>
        }
        <Modal
          isOpen={showError}
          className="duola-modal"
          overlayClassName="duola-modal-overlay"
          contentLabel="Err-Modal"
          style={customStyles}
          onRequestClose={this.onCloseModal}
        >
          <div className="error-modal-box">
            <p>{errorMessage}</p>
            <button className="error-modal-btn" onClick={this.onCloseModal}>确定</button>
          </div>
        </Modal>
      </div>
    )
 }
}
