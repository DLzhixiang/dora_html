
import React, { Component } from 'react'
import style from './style.scss'
import classnames from 'classnames'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import ProgressBar from 'progressbar.js'
import Header from '../../components/Header'
import Step from '../../components/Step'
import Modal from 'react-modal'
import store from 'store'

//打印成功！请取件！

let timeout = null, reboot = null, timer = null;
const customStyles = {
  overlay: {
    zIndex: 10
  }
};

@inject('duola') @observer
export default class extends Component {

  constructor(props){
    super(props)
    this.bar = null;
    this.state = {
      tip: 1,
      progress: 0.1,
      text: '正在准备打印... 0%'
    }
    this.tips = [
      '除了U盘打印，多拉还支持多拉APP取件哦～',
      '除了使用多拉APP取件，多拉还支持U盘打印哦～',
      '将要打印的文件转换成PDF打印质量更有保证哦～',
      '照片冲印需要时间较长，请耐心等候哦~'
    ]
  }

  componentWillmount(){
    this.props.duola.setPrintPercent(0.1);
  }

  componentDidMount() {
    // this.bar = new ProgressBar.Line(this.progress,{
    //   strokeWidth: 2,
    //   easing: 'easeInOut',
    //   duration: 1400,
    //   color: '#111',
    //   trailColor: 'rgba(0,0,0,0)',
    //   trailWidth: 2,
    //   svgStyle: {width: '100%', height: '100%'},
    //   text: {
    //     style: {
    //       // Text color.
    //       // Default: same as stroke color (options.color)
    //       color: '#111',
    //       width: '100%',
    //       textAlign: 'left',
    //       position: 'absolute',
    //       top: '0px',
    //       lineHeight: '44px',
    //       fontSize: '36px',
    //       padding: 0,
    //       margin: 0,
    //       mixBlendMode: 'difference',
    //       transform: {
    //         prefix: true,
    //         value: 'translate(0%, 30%)'
    //       }
    //     },
    //     autoStyleContainer: false
    //   },
    //   from: {color: '#FFEA82'},
    //   to: {color: '#ED6A5A'},
    //   step: (state, bar) => {
    //     let tip = '';
    //     if(bar.value() <= 0.1){
    //       tip = '正在准备打印...';
    //     }else if (bar.value() <= 0.7) {
    //       tip = '正在打印...';
    //     }else if (true) {
    //       tip = '正在清理您的个人文件，安全无忧～';
    //     }
    //     bar.setText(`${tip}${Math.round(bar.value() * 100)}%`);
    //     // bar.text.style.left = Math.round(bar.value() * 100) + '%';
    //   }
    // // });
    // this.bar.animate(this.props.duola.printPercent);
    // this.bar.animate(.5);
    this.setState({
      process: this.props.duola.printPercent,
    })
    timeout = setTimeout(()=>{
      this.props.duola.socket.socketSend("printTooLong");
      this.props.duola.fetchUpPrintingError({
        time: new Date(),
        orderId: this.props.duola.orderId,
        terminal: this.props.duola.user.namecode,
        errorCode: 0,
        errorDesc: '打印超时',
        file: ''
      },'printingError');
      console.log('打印超时');
      this.props.duola.setErrorModal(true,'打印超时！');
    }, 1000 * 870);//停留超过十五分钟手动检查
     reboot = setTimeout(()=>{
        this.props.duola.onSendMessage('REBOOT');
    }, 1000 * 900)
    timer = setInterval(()=>{
      this.setState({
        tip: ((this.state.tip == 3) ? 1 : (this.state.tip + 1))
      })
    },1000 * 10)

  }

  componentDidUpdate(prevProps, prevState) {
    const { printPercent } =  this.props.duola;
    let tip = ''
    if(printPercent <= 0.1){
      tip = '正在准备打印...';
    }else if (printPercent <= 0.7) {
      tip = '正在打印...';
    }else if (printPercent < 1) {
      tip = '正在清理您的个人文件，安全无忧～';
    }else {
      hashHistory.push('/success')
    }
    if(prevState.process != printPercent && prevState.process){
        this.setState({
          process: printPercent,
          text: `${tip}${Math.round(prevState.process * 100)}%`
        })
    }
    // this.bar && this.bar.animate(printPercent,()=>{
    //   if(printPercent >= 1){
    //     hashHistory.push('/success')
    //   }
    // });
    store.set('printPercent', printPercent)
  }

  componentWillUnmount() {
    // this.bar.destroy();
    this.props.duola.setPrintPercent(0.1);
    timeout && clearTimeout(timeout);
    reboot && clearTimeout(reboot);
    timer && clearInterval(timer);
    console.log('离开打印页');
    store.set('printPercent', null)
  }



  onCloseModal = () => {
    this.props.duola.setErrorModal(false,'')
    // hashHistory.replace('/')
  }

  render(){
    const { showError, errorMessage, printPercent, scanCode } = this.props.duola;
    return(
      <div >
        <Header hasBack={false} hasHelp={false} title="打印中"/>
        <div className={style['container']}>
          <div className={style['tips1']}>
            <img src={require("./assets/printing.png")} width="200" height="200"/>
            {
              this.props.location.state == "usb" ? (
                <p>呼哧～呼哧～多拉正在努力为您打印中</p>
              ) : (
                <p>呼哧~呼哧~多拉正在努力{scanCode.printIndex ? '打印':'加载'}您的第 { scanCode.printIndex||1 } / { scanCode.taskNumber } 份文件</p>
              )
            }


          </div>
          <div className={style['progress-box']} ref={e => this.progress = e }>
            <div className={style['border-box']}>
              <div className={style['whitebg']} />
              <div className={style['blackbg']} style={{width: this.state.progress}} />
              <div className={style['makebg']} />
              <span>{this.state.text}</span>
            </div>
          </div>
          <p className={style['tips2']}>
            多拉小贴士：
            <span>
              {
                (this.state.tip == 1) ? (
                  this.props.location.state == "usb" ? "除了U盘打印，多拉还支持多拉APP取件哦～" : "除了使用多拉APP取件，多拉还支持U盘打印哦～"
                ) : (
                  this.tips[this.state.tip]
                )
              }
            </span>
          </p>
        </div>
        <Modal
          isOpen={showError}
          className="duola-modal"
          overlayClassName="duola-modal-overlay"
          contentLabel="Err-Modal"
          style={customStyles}
          onRequestClose={this.onCloseModal}
        >
          <div className="error-modal-box">
            <p>{errorMessage}</p>
            <button className="error-modal-btn" onClick={this.onCloseModal}>确定</button>
          </div>
        </Modal>
      </div>
    )
 }
}
