
import React, { Component } from 'react'
import { Modal, Button } from 'antd';
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import { Spin } from 'antd'
import style from './style.scss'
import $ from 'jquery'
import Header from '../../components/Header'
import Step from '../../components/Step'
import Back from '../../components/Back'
import { clipPath } from '../../utils'
import QRCode from 'qrcode.react'


let timer = null;
@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      timeout: false,
      countDown: 90,
      order: null,
      paid: false
    }
  }

  componentDidMount(){
    this.onFetchOrder();
    this.onCountDown();
  }

  async onFetchOrder(){
    const { duola : { fetchOrder, operation }, location: { state } } = this.props;
    const order = await fetchOrder(state.orderData);
    operation.formatOrderInfo([order]);
    this.setState({
      order: order
    })
  }

  onCountDown(){
    let { countDown, order, paid } = this.state;
    timer = setInterval(()=>{
      this.setState({
        countDown: --countDown
      })
      if(countDown == 0){
        timer && clearInterval(timer)
        this.setState({timeout: true})
      }else if (this.state.order) {
          this.checkStatus(this.state.order.orderId).then(v =>{
            this.props.duola.setOrderId(this.state.order.orderId);
            if(v && !this.state.paid){
              this.setState({paid: true})
            }
          })
      }
      if(this.state.paid){
        if(this.props.duola.USBStatus == 1){
          this.props.duola.onSendMessage('DISKPRINTING','',this.props.location.state.printData)
          hashHistory.push({ pathname:'/guideout', state: 'end' });
        }else {
          this.props.duola.onSendMessage('DISKPRINTING','',this.props.location.state.printData)
          hashHistory.push({ pathname:'/print', state: 'usb' });
        }
      }
    },1000)
  }

   checkStatus(id){
    const result = this.props.duola.fetchPayStatus(id);
    return result;
  }

  onCancelPrintting(){
      hashHistory.goBack()
  }

  async onRepay(){
    this.onFetchOrder();
    await this.setState({
      timeout: false,
      countDown: 90
    })
    this.onCountDown();
  }

  componentWillUnmount(){
    timer && clearInterval(timer)
  }

  render(){
    const { state } = this.props.location;
    const { socket } = this.props.duola;
    const { order } = this.state;
    const orderData = state.orderData;
    const pageNum = Math.ceil(orderData.num * (orderData.endPage - orderData.startPage + 1) * (1/orderData.pagesPerSheet));
    return(
      <div className={style['container']}>
        <Header title="支付"/>
        <div className={style['content']}>
          <div className={style['pay-card']}>
              <p className={style['name']}>
              <img className={style['type-img']} src={require(`./assets/file_${orderData.fileName.split('.').pop().toLowerCase()}.png`)} style={{marginRight: 20}}/>
              { clipPath(orderData.fileName,18) } </p>
              <p className={style['cost']}>{pageNum}页 x {orderData.color == 1 ? (socket.blackWhitePrice * socket.blackWhiteDiscount) : (socket.colorPringPrice * socket.colorPringDiscount)}元/页（{orderData.printType == 1 ? '单' : '双'}面）</p>
              {
                order ?
                  (
                    <div>
                      <div className={style['qrcode-box']}>
                        <QRCode value={order ? order.alipay.credential.alipay_qr: ''} size={132} level="M" />
                        <img className={style['pay-icon']} src={require('./assets/alipay.png')}/>
                      </div>
                      <div className={style['qrcode-box']}>
                        <QRCode value={order ? order.wxpay.credential.wx_pub_qr: ''} size={132} level="M" />
                        <img className={style['pay-icon']} src={require('./assets/wxpay.png')}/>
                      </div>
                    </div>
                  )
                  :<Spin size="large" wrapperClassName={style['spin']}><div> </div></Spin>
              }
              <p className={style['countDown']}>请在<span style={{color: 'red'}}>{this.state.countDown}</span>秒内完成支付</p>
          </div>
          <Modal
            visible={this.state.timeout}
            closable={false}
            width={450}
            footer={null}
            wrapClassName="payModal"
            style={{textAlign: 'center',borderRadius: 0, top: 220}}
          >
            <p className={style['prompt-1']}>哎呀～支付超时！</p>
            <p className={style['prompt-2']}>如果点击“取消支付”，该次打印订单将被取消。</p>
            <div className={style['btn-group']}>
              <span onClick={()=>this.onCancelPrintting()}>
                取消支付
              </span>
              <span onClick={()=>this.onRepay()}>
                重新支付
              </span>
            </div>
          </Modal>
        </div>
        <Step style={{marginTop: '15vw'}} steps={['插入U盘','选择文件','打印设置','支付','完成打印']} step={3}/>
        <Back ev="取消支付"/>
      </div>
    )
 }
}
