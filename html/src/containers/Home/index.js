
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import Modal from 'react-modal'
import { trim } from '../../utils'
import Header from '../../components/Header'
import Download from '../../components/Download'
import $ from 'jquery'

const customStyles = {
  overlay: {
    zIndex: 10
  }
};

let timeout = null;

@inject('duola') @observer
export default class Home extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.duola.fetchOnReady();
    this.props.duola.scanCode.resetPrintStatus()
    if(this.props.duola.user.id == undefined){
      this.props.duola.setLoginVisible(true)
    }else{
      this.props.duola.fetchPrinter()
    }
    this.onWelcome()
  }

  onStart = () => {
    const { isPrinterReady, printer_id }= this.props.duola;
    if(!isPrinterReady || !printer_id)  return ;// this.props.duola.setErrorModal(true,'')
    hashHistory.push('/login')
  }

  onUSB = () => {
    const { isPrinterReady, printer_id }= this.props.duola;
    if(!isPrinterReady || !printer_id)  return ;// this.props.duola.setErrorModal(true,'')
    hashHistory.push({ pathname:'/guidein', state: 'begin' });
  }

  onFieldChange = (e) => {
    const name = e.target.name,
          value = e.target.value;
    this.props.duola.setField(name,value);
  }

  onSubmit = () => {
    const namecode = trim(this.namecode.value),
          secretcode = trim(this.secretcode.value);
    if( namecode =='' || secretcode == '')return;
    this.props.duola.fetchLogin(namecode,secretcode)
  }

  onCloseModal = () => {
    this.props.duola.setErrorModal(false,'')
  }

  onWelcome = () => {
    const { loginVisible }= this.props.duola;
    (!loginVisible) && (
      timeout = setTimeout(function () {
        hashHistory.push('/welcome')
        console.log('push welcome');
      }, 50 * 1000)
    )
  }

  resetTimeout(){
    timeout && clearTimeout(timeout);
    this.onWelcome();
  }

  componentWillUnmount(){
    timeout && clearTimeout(timeout)
  }

  render(){
    const { isPrinterReady, printer_id, loginVisible, showError, errorMessage, activity }= this.props.duola;
    return(
      <div onClick={()=>this.resetTimeout()}>
        <Header hasBack={false} hasShadow={false}/>
        <div className={style['container']}>
          <h2 className={style['title']}>Hi 我是多拉!</h2>
          <p className={style['sub-title']}>来自未来的自助云端打印机</p>
          <div className={style['btns-box']}>
            <span onClick={this.onStart} className={(!isPrinterReady)?style['disabled']:''}>扫码取件</span>
            <span onClick={this.onUSB} ><i>U</i>盘打印</span>
          </div>
        </div>
        <Download />
        <Modal
          isOpen={loginVisible}
          className="duola-modal"
          style={customStyles}
          overlayClassName="duola-modal-overlay"
          contentLabel="Modal"
        >
          <div className={style['login-box']}>
            <input type="text" placeholder="请输入用户名" ref={ e => this.namecode = e } />
            <input type="password" placeholder="密码" ref={ e => this.secretcode = e } />
            <button onClick={this.onSubmit}>登录</button>
          </div>
        </Modal>
        <Modal
          isOpen={showError}
          className="duola-modal"
          overlayClassName="duola-modal-overlay"
          contentLabel="Err-Modal"
          onClick={()=>this.resetTimeout()}
          onRequestClose={this.onCloseModal}
        >
          <div className="error-modal-box">
            <p>{errorMessage}</p>
            <button className="error-modal-btn" onClick={this.onCloseModal}>确定</button>
          </div>
        </Modal>
      </div>
    )
  }
}
          // onRequestClose={()=>{this.props.user.setLoginVisible(false)}}
