
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'

import Download from '../../components/Download'
import Header from '../../components/Header'
import Back from '../../components/Back'

let timeout = null

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
  }

  componentDidMount(){
    this.props.duola.socket.getTermGroup()
  }

  render(){
    return(
      <div>
        <Header hasBack={false} hasHelp={false} hasShadow={false}/>
        <div className={style['container']}>
          <div className={style['content']}>
            <span className={style.title}>附近的其他多拉打印机</span>
            <span className={style['sub-title']}> 这台设备正在维护中，您可以选择附近其他终端进行打印 </span>
            <ul className={style.printers}>
                {
                  this.props.duola.socket.canUse && this.props.duola.socket.canUse.map((item, index)=>(
                    <li key={index}>
                        {item.address}
                    </li>
                  ))
                }
            </ul>
          </div>
          <p className={style['bottom-box']}>
            <span className={style['left-box']}>
            </span>
            <span className={style['right-box']}>
              <img src={require('./assets/app.png')} height="122"/>
            </span>
          </p>
        </div>
        <Back ev="离开打印机列表"/>
      </div>
    )
 }
}
