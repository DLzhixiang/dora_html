
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import { message, Radio, Button } from 'antd'
const RadioGroup = Radio.Group;
import { apis } from '../../apis'
import $ from 'jquery'

import Download from '../../components/Download'
import Header from '../../components/Header'
import Back from '../../components/Back'

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      hasLogin: false,
      input: [],
      error: false
    }
  }

  handlerInput = async(code) => {
    const { input } = this.state;
    if(input.length < 3){
      this.setState({
        input: [...input, code]
      })
    }else if (input.length < 4) {
      await this.setState({
        input: [...input, code]
      })
      this.checkCode()
    }
  }

  checkCode = () => {
      if(JSON.stringify(this.state.input) == JSON.stringify([3,3,3,2])){
        this.setState({
          hasLogin: true
        })
      }else {
        this.setState({
          input: [],
          error: true
        })
        setTimeout(()=>{
          this.setState({
            error: false
          })
        },3000)
      }
  }

  componentDidMount(){
  }

  componentWillUnmount() {
  }

  handlerSubmit = async() => {
      if(Object.values(this.state).length > 2){
        const { photoPage, paperPage1, paperPage2, paperPage3 } = this.state;
         let  parmas = {
                terminalId: this.props.duola.user.id,
                paperPage: Number(paperPage1 || 0) + Number(paperPage2 || 0) + Number(paperPage3 || 0),
                photoPage
              };
         const res = await $.post(`${apis.API_URL}${apis.setPaperCount}`,parmas);
         if(res.code == 0){
            hashHistory.goBack()
         }else{
            message.error(res.message, 3)
         }
      }
  }

  handlerSetSupplies = (val, supplies ) => {
      let obj = {};
      obj[supplies] = val.target.value;
      this.setState(obj)
  }

  render(){
    const { user } = this.props.duola;
    const { hasLogin, input, error } = this.state;
    return(
      <div>
        <Header hasBack={false} hasHelp={false} hasShadow={false} title="运维维护"/>
        <div className={style['container']}>
            {
              hasLogin ? (
                <div className="operation-box">
                  <p className={style.title}>请对本次添加纸量进行选择</p>
                  <p className={style.warning}>非多拉工作人员，请勿选择</p>
                  <RadioGroup onChange={(e)=>this.handlerSetSupplies(e, 'photoPage')} >
                     &nbsp;&nbsp;&nbsp;相纸:
                    <Radio value={0}>未添加</Radio>
                    <Radio value={15}>已填满</Radio>
                  </RadioGroup>
                  <RadioGroup onChange={(e)=>this.handlerSetSupplies(e, 'paperPage1')} >
                    纸盒1:
                    <Radio value={0}>未添加</Radio>
                    <Radio value={100}>已填满</Radio>
                  </RadioGroup>
                  <RadioGroup onChange={(e)=>this.handlerSetSupplies(e, 'paperPage2')} >
                    纸盒2:
                    <Radio value={0}>未添加</Radio>
                    <Radio value={100}>已填满</Radio>
                  </RadioGroup>
                  <RadioGroup onChange={(e)=>this.handlerSetSupplies(e, 'paperPage3')} >
                    纸盒3:
                    <Radio value={0}>未添加</Radio>
                    <Radio value={100}>已填满</Radio>
                  </RadioGroup>
                  <Button className={(Object.values(this.state).length > 2) ? style['commit_btn'] : style['commit_btn_disabled']} onClick={this.handlerSubmit}>签到提交</Button>
                </div>
              ): (
                <div>
                    <p className={style.title}>请输入密码</p>
                    <div></div>
                    <ul className={style.password}>
                      {
                        [1,2,3,4].map((val,index)=>{
                          if (input[index]) {
                            return (
                              <li key={val}>
                                <span className={style.asterisk}>
                                  *
                                </span>
                              </li>
                            )
                          } else if(input[index-1]|| index == 0){
                            return (
                              <li key={val}>
                                <span className={style.blink}>
                                  |
                                </span>
                              </li>
                            )
                          }else{
                            return (
                              <li key={val}>

                              </li>
                            )
                          }
                        })
                      }
                    </ul>
                    <p className={style.warning} style={{visibility: error ? '' : 'hidden'}}>密码填写错误，请重新填写</p>
                    <ul className={style.code}>
                      {
                        [1,2,3,4].map((val,index)=>(
                          <li key={val} onClick={()=>this.handlerInput(val)}>
                            {val}
                          </li>
                        ))
                      }
                    </ul>
                </div>
              )
            }
            <Back />
        </div>
      </div>
    )
 }
}
