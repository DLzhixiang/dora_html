export const data = {
    platform: {
      1: 'app',
      2: 'app',
      3: 'web',
      4: 'wechat',
      5: 'external',
      6: 'Udisk'
    },
    type: {
      1: 'doc',
      2: 'color',
      4: 'photo',
      8: 'id'
    },
    pagesPerSheet: {
      1: 'oneINone',
      2: 'multiINone',
      4: 'multiINone',
      6: 'multiINone'
    },
    printType: {
      1: "isNotDuplex",
      2: 'isDuplex',
      3: 'isDuplex'
    },
    isImage: {
      0: 'isNotImage',
      1: 'isImage'
    },
    isWord: {
      0: 'isNotWord',
      1: 'isWord',
    },
    isPhoto: {
      0: 'notPhoto',
      1: 'isWechatPhoto',
      2: 'isAppPhoto'
    },
    isUdiskPhoto: {
      0: 'isNotUdiskPhoto',
      1: 'isUdiskPhoto',
    }
}

export const map = {
  doc: ['墨浅/没墨了','漏墨/有墨点','打印出白纸','完全不出纸','只印出部分','纸质不好','操作不方便','价格太贵','纸张褶皱'],
  color: ['墨浅/没墨了','漏墨/有墨点','打印出白纸','完全不出纸','只印出部分','纸质不好','操作不方便','价格太贵','纸张褶皱'],
  photo: ['完全不出相纸','只印出部分照片','照片有色差','照片画质模糊','照片冲印太慢','操作不方便','价格太贵','功能太少','白边不对称'],
  id: ['完全不出相纸','只印出部分照片','照片有色差','照片画质模糊','照片冲印太慢','操作不方便','价格太贵','功能太少'],
  app: ['设置选择少'],
  Udisk: ['U盘插口松', '不能批量打印'],
  multiINone: ['缩印排版不对'],
  wechat: ['打印机不好找', '不能批量打印'],
  isWord: ['页数或字体错乱'],
  isDuplex: ['翻页方式不对'],
  isWechatPhoto: ['只能印9张'],
  isAppPhoto: ['只能印15张'],
  isUdiskPhoto: ['没有缩略图','不能冲印照片']
}

export const goodPhoto = ['照片质量好','操作简便','立等可取','机器易寻找'];
export const goodDoc = ['操作简便','不用排队','位置优越','随时可以用'];
export const rateDes = ['','不能忍','很差','待提高','满意','很满意'];

export const tagList = ['墨浅/没墨了','漏墨/有墨点','打印出白纸','完全不出纸','只印出部分','纸质不好','操作不方便','纸张褶皱','完全不出相纸','只印出部分照片','照片有色差','照片画质模糊','照片冲印太慢','价格太贵','功能太少','白边不对称','设置选择少','U盘插口松', '不能批量打印','缩印排版不对','打印机不好找', '不能批量打印','页数或字体错乱','翻页方式不对','只能印9张','只能印15张','没有缩略图','不能冲印照片','照片质量好','操作简便','立等可取','机器易寻找','不用排队','位置优越','随时可以用'];
