
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import classnames from 'classnames'
import { trim, shuffle } from '../../utils'
import Header from '../../components/Header'
import Back from '../../components/Back'
import Download from '../../components/Download'
import { Radio, Button, Rate, Checkbox, Row, Col } from 'antd';
import $ from 'jquery'
import { apis } from '../../apis'
import TweenOne from 'rc-tween-one';
const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;
import { data, map, goodPhoto, goodDoc, rateDes, tagList } from './data'


let timeout  = null;

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      step: 1,
      rate: null,
      tags: [],
      selectTags: []
    }
    this.up = { y: '-14vw', duration: 100 };
    this.hidden = { opacity: 0, duration: 100}
    this.show = { opacity: 1, duration: 300, delay: 1000}
  }

  componentWillMount() {
    const { operation: { orderInfo } } = this.props.duola;
    // let orderInfo = { paperType: 1, type: 1, pagesPerSheet: 1, paperType: 1, isWord: 1, isPhoto: 1, isUdiskPhoto: 1}
    let orderType = Object.keys(orderInfo).map((key)=>data[key][orderInfo[key]]);
    let filteredTags = orderType.reduce((sum,cur)=>(
      map[cur] ? sum.concat(map[cur]) : sum
    ),[]);
    let tags = [];
    for (let i = 0; i < 5; i++) {
      switch (i) {
        case 4:
          (orderInfo.type < 4) ? (tags[i] = shuffle(goodDoc)) :( tags[i] = shuffle(goodPhoto))
        break;
        default:
          tags[i] = shuffle(filteredTags)
          break;
      }
    }
    this.setState({tags: tags})
  }

  onChange = (val) => {
    let filteredVal = val;
    if(val[val.length-1] == '完全不出纸'){
      filteredVal = filteredVal.filter((tag)=> tag != '只印出部分')
    }else if (val[val.length-1] == '只印出部分') {
      filteredVal = filteredVal.filter((tag)=> tag != '完全不出纸')
    }
    if(val[val.length-1] == '只印出部分照片'){
      filteredVal = filteredVal.filter((tag)=> tag != '完全不出相纸')
    }else if (val[val.length-1] == '完全不出相纸') {
      filteredVal = filteredVal.filter((tag)=> tag != '只印出部分照片')
    }
    this.setState({
      selectTags: filteredVal
    });
  }

  handlerRate = (value) => {
    this.setState({
      rate: value,
      selectTags: []
    });
  }

  handlerSubmit = async() => {
    const { rate, selectTags } = this.state;
    if(rate){
      hashHistory.push('/success')
      let res = tagList.map((val)=>{
              return selectTags.includes(val) ? val : null
          })
      const parmas = {
        feedType: rate,
        feedMessage: res,
        orderId: this.props.duola.orderId,
        version: 1
      }
      const result = await $.get(`${apis.API_URL}${apis.feedback}`,parmas);
    }else {
      hashHistory.push('/')
    }
  }

  goHelp = () => {
    this.props.duola.onChangeHelpTab(' customercare')
    hashHistory.push('/help')
  }

  render(){
    const { rate, tags, selectTags } = this.state;
    const { type } = this.props.duola.operation.orderInfo;
    return(
      <div className={style.container}>
        <Header title={'打印成功'}/>
            <div>
              <div className={style['content']}>
                  <TweenOne
                    animation={rate && this.hidden}
                  >
                    <h3 className={style.title}>打印成功，请对多拉本次表现进行评价~</h3>
                  </TweenOne>
                  <TweenOne
                    animation={rate && this.up}
                    className={style['rate-box']}
                  >
                    <Rate className={style.rate} onChange={this.handlerRate}/>
                    <p className={style.des}>{rateDes[rate]}</p>
                    <TweenOne
                      animation={rate && this.show}
                      style={{opacity: 0}}
                    >
                        <Checkbox.Group onChange={this.onChange} value={selectTags}>
                          <Row className="tag-box">
                          {
                            rate && tags[rate-1].map((tag,index)=>(
                              rate != 5 ? (<Col span={4} offset={index % 5 == 0 ? 0 : 1} key={index}><Checkbox value={tag} disabled={rate ? false : true}>{tag}</Checkbox></Col>)
                                        :  <Col span={6} offset={index % 2 == 0 ? 5 : 2} key={index}><Checkbox value={tag} disabled={rate ? false : true}>{tag}</Checkbox></Col>
                            ))
                          }
                          </Row>
                        </Checkbox.Group>
                    </TweenOne>
                  </TweenOne>
              </div>
           </div>
        <footer>
          <a onClick={this.goHelp}>立即联系客服</a>
          <Button className={style['commit_btn']} onClick={this.handlerSubmit}>{rate ? '提交' : '残忍拒绝评价'}</Button>
        </footer>
        <Back home/>
      </div>
    )
  }
}
