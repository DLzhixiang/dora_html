import React, { Component } from 'react'
import { Form, Input, Icon,  Checkbox, Button, Radio, Select, Modal } from 'antd';
import { PickerView } from 'antd-mobile';
import classnames from 'classnames'
import { inject, observer } from 'mobx-react'
import { autorun } from 'mobx';
import { Link, hashHistory } from 'react-router'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
import style from './printSettingForm.scss'
import { printRange, outOfRangeText, printTypeConf, paperPerSheetConf, imgType, pdfType, excelType } from '../../utils/config'
import { getFileTypeByName } from '../../utils'

let fileCur = "";

@inject('duola') @observer
class PrinttingForm extends React.Component {
  constructor(){
    super()
    this.state = {
      num: 1,
      printType: 1,
      startPage: 1,
      endPage: 1,
      fileName: '',
      range: 'all',
      color: 1,
      pages: null,
      pagesPerSheet: 1,
      layoutIsVisible: false,
      printTypeIsVisible: false,
      noNInOne: true
    };
  }

  componentWillMount() {
    this.handler = autorun(() => {
      if(this.props.duola.fileInfo.file_path != fileCur){
        this.setState({
          endPage: Number(this.props.duola.fileInfo.pages) || 1,
          startPage: 1,
          range: 'all',
          printType: 1,
          printTypeIsVisible: false,
          printTypeIsVisible: false,
          num: 1
        });
        fileCur = this.props.duola.fileInfo.file_path;
      }
      if(this.props.duola.fileInfo.file_path == 0){

      }
    });
  }


  componentDidUpdate(prevProps, prevState) {
    if(this.props.info != prevProps.info && this.props.info){
      this.setState({
        noNInOne: imgType.concat(pdfType, excelType).indexOf(getFileTypeByName(this.props.info.name).toLowerCase()) > -1,
        printTypeIsVisible: false,
        layoutIsVisible: false,
        pagesPerSheet: 1,
        printType: 1,
        num: 1
      })
    }
  }

  componentWillUnmountMount() {
    this.handler = null;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { copyStatus, onSendMessage, fileInfo } = this.props.duola;
    const { num, startPage, endPage, printType, pagesPerSheet, color } = this.state;
    const pageNum = Math.ceil(num * (endPage - startPage + 1) * (printType == 1 ? 1 : 1/2) * (1/pagesPerSheet));
    const inRange = (pageNum <= printRange);
    if(copyStatus && inRange && this.props.duola.fileInfo.pages){
      let printData = {
          "file": fileInfo.file_path,
          "copies": num,
          "duplex": printType,
          "from": startPage,
          "to": endPage,
          "pages_per_sheet": pagesPerSheet,
          "color": color
      };
      let orderData = {
        "gid": this.props.duola.user.gid,
        "terminalId": this.props.duola.user.id,
        "fileName": this.props.info.name,
        "totalPage": Number(this.props.duola.fileInfo.pages),
        "num": num,
        "printType": printType,
        "startPage": startPage,
        "endPage": endPage,
        "pagesPerSheet": pagesPerSheet,
        "color": color,
        "type": color,
        "userId": -1,
        "dataUrl": -1,
        "paperType": 1,
        "platform": 2
      };
      hashHistory.push({
        pathname:'/pay',
        state: {
          orderData: orderData,
          printData: printData
        }
      });
    }
  }

  onChangeNum = (flag) => {
    if(flag && this.state.num < 100){
      this.setState({num: ++this.state.num})
    }else if(!flag && this.state.num > 1){
      this.setState({num: --this.state.num})
    }
  }

  onChangePrintType = (value) => {
    if((this.state.startPage - this.state.endPage) == 0) return;
    this.setState({
      printTypeIsVisible: !this.state.printTypeIsVisible,
      layoutIsVisible: false,
      printType: value ? value : this.state.printType
    })
  }

  onChangeColorType = (value) => {
      if(!this.props.duola.socket.hasColor) return;
      this.setState({
        color: value
      })
  }

  onChangePageSize = () => {
  }

  onChangeStartPage = (value) => {
    const { startPage, endPage} = this.state;
    Number(value) >= endPage ? this.setState({
      startPage:Number(value),
      endPage: Number(value),
      printType: 1
    }) : this.setState({startPage: Number(value)})
  }

  onChangeEndPage = (value) => {
    const { startPage, endPage} = this.state;
    Number(value) <= startPage ? this.setState({
      startPage:Number(value),
      endPage: Number(value),
      printType: 1
    }) : this.setState({endPage: Number(value)})
  }

  onChangeDirection = (value) => {
    // this.setState({direction: value})
  }

  onChangeRange = (value) => {
    this.setState({range: value})
    if(value == 'all'){
      this.setState({
        startPage: 1,
        endPage: Number(this.props.duola.fileInfo.pages) || 1
      })
    }else {
    }
  }

  onChangeLayout(value){
      if(this.state.noNInOne) return;
      this.setState({
        layoutIsVisible: !this.state.layoutIsVisible,
        printTypeIsVisible: false,
        pagesPerSheet: value ? value : this.state.pagesPerSheet
      })
  }

  render() {
    const { num, printType, startPage, endPage, direction, range, pagesPerSheet, noNInOne, color } = this.state;
    const { socket } = this.props.duola;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    const pageNum = Math.ceil(num * (endPage - startPage + 1)  * (1/pagesPerSheet));
    const inRange = ((pageNum * (printType == 1 ? 1 : 1/2)) <= printRange);
    return (
        <div className={style['content']}>
          <Form onSubmit={this.handleSubmit} >
            <FormItem
              {...formItemLayout}
              hasFeedback
            >
              <p className={style['title']}>打印设置</p>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="打印份数"
              hasFeedback
            >
              <div className={num != 1 ? style['minus'] : style['minus-o']} onClick={()=>this.onChangeNum(false)} />
              <span className={style['num']} > { num } </span>
              <div className={num != 100 ? style['plus'] : style['plus-o']} onClick={()=>this.onChangeNum(true)} />
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="双面打印"
              hasFeedback
            >
                <div className={style['printType-config']}>
                  <div className={classnames(style['printType-selected'], (startPage == endPage || noNInOne) ? style.disabled : null)} onClick={()=>this.onChangePrintType()}>{printTypeConf[printType-1].type}</div>
                  <ul style={{display: this.state.printTypeIsVisible ? 'block' : 'none'}} >
                    {
                      printTypeConf.map((item, i)=>(
                        <li key={i} onClick={()=>this.onChangePrintType(item.key)}>
                            {item.type}<br />
                            {item.des}
                        </li>
                      ))
                    }
                  </ul>
                </div>
                {/* <div className={printType == 2 ? style['checked'] : style['unchecked']} onClick={()=>this.onChangePrintType()}></div> */}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="彩色"
              hasFeedback
            >
              <div className={color == 1 ? style['checked'] : style['unchecked']} onClick={()=>this.onChangeColorType(1)}></div><p className={style['value']}>黑色</p>
              <div className={color == 2 ? style['checked'] : style['unchecked']} onClick={()=>this.onChangeColorType(2)}></div><p className={style['value']}>彩色{
                socket.hasColor ? '' : <span className={style['tip']}>（设备暂时仅支持黑白）</span>
              }</p>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="纸张"
              hasFeedback
            >
            <div className={style['checked']} onClick={this.onChangePageSize}> </div><p className={style['value']}>A4<span className={style['tip']}>（210 x 297mm／设备暂时仅支持A4纸张）</span></p>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="打印范围"
            >
              <div className={range == 'all' ? style['checked'] : style['unchecked']} onClick={()=>{this.onChangeRange('all')}}></div><p className={style['value']}>全部</p><br />
              <div className={range == 'custom' ? style['checked'] : style['unchecked']} onClick={()=>{this.onChangeRange('custom')}}></div>
              <div className={style['value']}>
                从 <Select size="large" value={startPage.toString()} style={{ width: 80 }} dropdownStyle={{maxHeight: 200, overflow: 'scroll'}} onChange={this.onChangeStartPage}  disabled={range == 'all'}>
                  { this.props.duola.fileInfo.pages ? [...Array(this.props.duola.fileInfo.pages*1).keys()].map((item, i) => <Option key={i+1}>{i+1}</Option>) : <Option key={1}>1</Option> }
                </Select>
                至 <Select size="large" value={endPage.toString()} style={{ width: 80 }} dropdownStyle={{maxHeight: 200, overflow: 'scroll'}}  onChange={this.onChangeEndPage} disabled={range == 'all'}>
                  { this.props.duola.fileInfo.pages ? [...Array(this.props.duola.fileInfo.pages*1).keys()].map((item, i) => <Option key={i+1} >{i+1}</Option>).filter((item, i)=> i + 1 >= this.state.startPage ) : <Option key={1}>1</Option> }
                </Select>
              </div>
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="布局"
              hasFeedback
            >
            <div className={style['layout-config']}>
              <div className={classnames(style['layout-selected'], noNInOne ? style.disabled : null)}onClick={()=>this.onChangeLayout()}>每版打印{pagesPerSheet}页</div>
              <ul style={{display: this.state.layoutIsVisible ? 'block' : 'none'}}>
                {
                  [1,2,4,6].map((item, i)=>(
                    <li key={item} onClick={()=>this.onChangeLayout(item)}>
                        每版打印{`${item}`}页
                    </li>
                  ))
                }
              </ul>
            </div>
            </FormItem>
            <FormItem style={{marginTop: 50}}>
              <p className={style['cost']}>总价：{color == 1 ? (pageNum * socket.blackWhitePrice * socket.blackWhiteDiscount).toFixed(1) : (pageNum * socket.colorPringPrice * socket.colorPringDiscount).toFixed(1)}元 <br/> <span> {pageNum}页 x {color == 1 ? socket.blackWhitePrice * socket.blackWhiteDiscount : (socket.colorPringPrice * socket.colorPringDiscount).toFixed(1)}元/页（{printType == 1 ? '单' : '双'}面)</span></p>
              {!inRange &&
                <p className={style['out-range']}>{
                  printType == 1 ? outOfRangeText['oneSide'] : outOfRangeText['twoSide']
                }</p>
              }
              {
                this.props.duola.copyStatus
                ?<Button htmlType="submit" className={(this.props.duola.copyStatus && inRange && this.props.duola.fileInfo.pages) ? style['print-btn'] : style['print-btn-disabled']}>{this.props.duola.fileInfo.pages ? '确认打印' : '该文档无法打印'}</Button>
                :<Button htmlType="submit" className={style['loading-btn']}>加载中</Button>
              }
            </FormItem>
          </Form>
        </div>
    );
  }
}

export default Form.create()(PrinttingForm);
