
import React, { Component } from 'react'
import style from './style.scss'
import classnames from 'classnames'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import { Row, Col, Breadcrumb, Icon} from 'antd';
import { autorun } from 'mobx';
import _ from 'lodash';
import $ from 'jquery';
import DisplayMode from '../../components/DisplayMode'
import Back from '../../components/Back'
import Step from '../../components/Step'
import FileInfo from '../../components/FileInfo'
import PrintSettingForm from './printSettingForm'
import { trim, bytesToSize, clipPath } from '../../utils'
import Header from '../../components/Header'
// import usbDriveData from '../../datasource.json'

const config = {
  max: 3,
  space_x: -200,
  space_y: -133
}

let sortTypeCur = "time"

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      data: [],
      top: -511,
      // top: 0,
      selectedFile: null,
    }
  }

  async componentDidMount() {
    const { usbDriveData } =  this.props.duola;
    this.props.duola.socket.getTermGroup()
    this.handler = autorun(() => {
        if(this.props.duola.sortType == 'time' && sortTypeCur == 'time'){
          const newData =  this.state.data.map((item,i)=>{
            return {...item,files: _.orderBy(item.files, function(o) { return o.last_modified; }, 'desc'), folders: _.orderBy(item.folders, function(o) { return o.last_modified; }, 'asc')}
          })
          this.setState({
            data: newData
          })
          sortTypeCur = 'case'
        }else if(this.props.duola.sortType == 'case' && sortTypeCur == 'case'){
          const newData =  this.state.data.map((item,i)=>{
            return {...item,files: _.orderBy(item.files, function(o) { return o.name; }),folders: _.orderBy(item.folders, function(o) { return o.name; }, 'asc')}
          })
          this.setState({
            data: newData
          })
          sortTypeCur = 'time'
        }
    });
    if(usbDriveData.length == 1){
        await this.setState({
          data: [{...usbDriveData[0],  files: _.orderBy(usbDriveData[0].files, (o)=> o.last_modified, 'desc'), folders: _.orderBy(usbDriveData[0].folders, (o)=> o.last_modified, 'asc')}]
        })
        let length = $('#stack li').length - 1;
        $('#stack li').each(function(n){
            this.style['-webkit-transform'] = `translate3d(${ config.space_x*(length - n)}px,  0px,  ${config.space_y*(length - n)}px)`;
        })
    }
  }

  componentWillUnmount() {
    this.handler = null;
  }

  addPage = async (newData) => {
    await this.setState({
      data: [...this.state.data, {...newData,  files: _.orderBy(newData.files, (o)=> this.props.duola.sortType == 'time' ? o.last_modified : o.name,this.props.duola.sortType == 'time' ? 'desc' : 'asc'), folders: _.orderBy(newData.folders, (o)=> this.props.duola.sortType == 'time' ? o.last_modified : o.name, 'asc')}],
      selectedFile: null
    })
    let length = $('#stack li').length - 1;
    setImmediate(()=>{
      $('#stack li').each(function(n){
          this.style['-webkit-transform'] = `translate3d(${config.space_x*(length - n)}px,  0px,  ${config.space_y*(length - n)}px)`;
          if(length - n == config.max){
             this.style['opacity'] = 0;
            //  this.style['display'] = 'none';
            this.addEventListener("transitionend", function end(){
              this.style['display'] = 'none';
              this.removeEventListener("transitionend", end)
            }, false);
          }else if(length == n){
            this.style['opacity'] = 1;
          }else if (length - n < config.max) {
            this.style['opacity'] = 0.5;
          }
      })
    })
  }

  removePage = (index) => {
   if(this.state.data.length < 2)return ;
   let self = this;
   $('#stack li').each(function(n){
       if(n > index){
          this.style['opacity'] = 0;
          this.addEventListener("transitionend", function end(){
            self.setState({
              data: self.state.data.slice(0,n),
              selectedFile: null
            })
            this.removeEventListener("transitionend", end)
          }, false);
       }else if(index == n){
          this.style['display'] = 'block';
          this.style['opacity'] = 1;
       }else if (index - n  < config.max) {
           this.style['display'] = 'block';
           this.style['opacity'] = .5;
       }
       this.style['-webkit-transform'] = `translate3d(${config.space_x*(index - n)}px,  0px,  ${config.space_y*(index - n)}px)`;
   })
  }

  showSetting = ()=>{
    let  filePath = "";
    this.state.data.map((item, i)=>{
      filePath += `${item['name']}\\`;
    })
    filePath += this.state.selectedFile.name;
    this.props.duola.onReselect();
    this.props.duola.onSendMessage('FILEPAGES','',{"file": filePath});
    this.setState({
      top: 0
    })
    $('#stack').hide()
  }

  selectFile = (file) => {
    this.setState({
      selectedFile: file
    })
  }

  customClick(){
    if(this.state.top == 0){
      this.setState({
        top: -511
      })
      $('#stack').show()
    }else {
      hashHistory.push('/')
    }
  }

  render(){
    const { showType, sortType, usbDriveData } = this.props.duola; //usbDriveData
    const { data } = this.state;
    return(
      <div className="container">
        <Header title = 'U盘打印'/>
        {
        data.length == 0 ? <div style={{textAlign: 'center', fontSize: '2rem', paddingTop: '200px', height: '633px'}}>
                              {
                                usbDriveData.map((item,index)=>
                                  <div style={{display: 'inline-block', margin: 40}} key={index} onClick={()=>this.addPage(usbDriveData[index])}>
                                      <img src={require('./assets/drive.png')}/>
                                      <p> {item.name} </p>
                                  </div>)
                              }
                             </div> :
          <Row className={style['content']}>
              <div className={style['view']}>
                  <ul className={style['stack']} id="stack">
                    {data.map((item, index)=>(
                      <li className={style['item']} key={index} onClick={this.state.data.length-1 !=index && (()=>this.removePage(index))}>
                          <div className={style['file-info']}>
                            <FileInfo onClick={this.showSetting} info={this.state.selectedFile}/>
                          </div>
                          <div className={classnames(style['file-list'],'file-list')} >
                          {
                            showType == 'column'
                                 ? (
                                   data[index]['files'].map((item, index)=>{
                                   return <div key={index} className={style['column-box']} onClick={()=>this.selectFile(item)}>
                                            <span className={style['icon-box']}><img src={require(`./assets/file_${item.name.split('.').pop().toLowerCase()}.png`)} className={style['file-icon']} height={37}/></span>
                                            <span className={style['name']}>{clipPath(item.name,26)}</span>
                                            <span className={style['size']}>{bytesToSize(item.size)}</span>
                                            <span className={style['time']}>{item.last_modified}</span>
                                          </div>
                                   }).concat(
                                    Object.keys(data[index]['folders']).map((folder, f_n)=>(
                                          <div key={'folder'+f_n} className={style['column-box']} onClick={()=>this.addPage(data[index]['folders'][folder])}>
                                               <span className={style['icon-box']}><img src={require('./assets/folder.png')}className={style['file-icon']}  height={37}/></span>
                                               <span className={style['name']}>{data[index]['folders'][folder].name}</span>
                                          </div>
                                    ))
                                   )
                                 )
                                 : (
                                     data[index]['files'].map((item, index)=>{
                                     return <div key={index} className={style['row-box']} onClick={()=>this.selectFile(item)}>
                                              <div className={style['img-box']}>
                                                <img src={require(`./assets/file_${item.name.split('.').pop().toLowerCase()}.png`)} className={style['file-icon']} height={66}/>
                                              </div>
                                              <span>{clipPath(item.name,8)}</span>
                                            </div>
                                     }).concat(
                                      Object.keys(data[index]['folders']).map((folder,f_n)=>(
                                            <div key={'folder'+f_n} className={style['row-box']} onClick={()=>this.addPage(data[index]['folders'][folder])}>
                                                 <div className={style['img-box']}>
                                                  <img src={require('./assets/folder.png')} height={66}/>
                                                 </div>
                                                 <span>{data[index]['folders'][folder].name}</span>
                                            </div>
                                      ))
                                     )
                                   )
                          }
                          </div>
                          <div className={style['nav']}>
                              <div className={style['arrow-group']}>
                                <span className={style['arrow']} onClick={(()=>this.removePage(this.state.data.length - 2))}> </span>
                              </div>
                              <Breadcrumb className={style['breadcrumb']}>
                                {
                                  this.state.data.slice(0,index + 1).map((item,crumbId)=>{
                                    return  <Breadcrumb.Item onClick={()=>this.removePage(crumbId)} key={crumbId}>{data[crumbId].name}</Breadcrumb.Item>
                                  })
                                }
                              </Breadcrumb>
                          </div>
                      </li>
                    ))}
                    <DisplayMode />
                  </ul>
              </div>
              <div className={style['setting']} style={{top: this.state.top}}>
                  <Row>
                    <Col lg={9}>
                      <FileInfo type="setting" info={this.state.selectedFile}/>
                    </Col>
                    <Col lg={15}>
                        <PrintSettingForm info={this.state.selectedFile} />
                    </Col>
                  </Row>
              </div>
          </Row>
        }
        <Back customClick={()=>this.customClick()}/>
        <Step style={{marginTop: '15vw'}} steps={['插入U盘','选择文件','打印设置','支付','完成打印']} step={this.state.top == 0 ? 2 : 1}/>
      </div>
    )
  }
}
