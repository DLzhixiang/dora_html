
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'

// import { errType} from '../../utils/config'
import Download from '../../components/Download'
import Header from '../../components/Header'
import data from './data'

let timeout = null;
@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      content: 'goAwry',
      count: 0,
      errorMessage: '维护中...'
    }
  }

  componentDidMount(){
    if(this.props.location && this.props.location.state){
      this.props.location.state.map((err)=>{
        if(errType[err]){
          this.setState({
            errorMessage: errType[err]
          })
        }
      })
    }
    timeout = setTimeout(()=>{
      this.setState({
        content: 'error'
      })
    }, 30 * 1000)
  }

  componentWillUnmount() {
    timeout && clearTimeout(timeout)
  }

  handlerRefund = () => {
    hashHistory.push('refund')
  }

  render(){
    const { user, operation } = this.props.duola;
    return(
      <div>
        <Header hasBack={false} hasHelp={false} hasShadow={false}/>
        <div className={style['container']}>
          {
            this.state.content == 'goAwry' ? (
              <div className={style['content']}>
                <span className={style.title}><img src={require('./assets/cartoon.png')} width="120" className={style.cartoon} style={{display: (this.state.errorMessage == "出现了一个Bug...") ? 'none' : 'inline-block'}}/>{this.state.errorMessage}</span>
                <span className={style['sub-title-goAwry']}> 本次任务已取消，您可以前往其他多拉打印机重新打印。<span className={style.refund} onClick={this.handlerRefund} style={{display: operation.orderInfo.platform ? '' : 'none'}}>(如何退款）</span></span>
              </div>
            ) : (
              <div className={style['content']}>
                <span className={style.title}><img src={require('./assets/tool.png')} className={style['tool']}/>设备维护中!</span>
                <span className={style['sub-title']}>
                不好意思！这台设备正在维护中，您可以选择附近其他终端进行打印<br />
                如有任何疑问您也可以联系多拉客服，以获得更多帮助
                </span>
              </div>
            )
          }
          <p className={style['button-box']}>
              <a onClick={this.goPrinterList}>
                查看附近打印机
              </a>
              <a onClick={this.goHelp}>
                联系客服
              </a>
          </p>
          <p className={style['bottom-box']}>
            <span className={style['left-box']}>
              <img src={require('./assets/help.png')} height="40" className={style.help} onClick={()=>this.goHome()}/>
              多拉24小时客服电话：400-160-3672
            </span>
            <span className={style['right-box']}>
              <img src={require('./assets/app.png')} height="122" onClick={()=>this.goOperation()}/>
            </span>
          </p>
        </div>
      </div>
    )
 }

 goHome(){
   this.setState({
     count: ++this.state.count
   })
   this.state.count > 15 && hashHistory.push('')
 }

 goOperation(){
   this.setState({
     count: ++this.state.count
   })
   this.state.count > 7 && hashHistory.push('operation')
 }

 goHelp = () => {
   this.props.duola.onChangeHelpTab(' customercare')
   hashHistory.push('/help')
 }

 goPrinterList = () => {
   hashHistory.push('/printerlist')
 }
}

// <p className={style['first']}>微信扫码关注多拉打印，</p>
// <p>可以通过快速客服通道快速解决退款等问题～</p><br />
// <p>客服电话：400-160-3672</p>
