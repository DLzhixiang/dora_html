import React, { Component } from 'react'
import style from './style.scss'
import classnames from 'classnames'
import { Link, hashHistory } from 'react-router'
import Header from '../../components/Header'
import $ from 'jquery'
import { inject, observer } from 'mobx-react'
import { apis } from '../../apis'
import { version } from '../../../package.json'

let timer = null;
@inject('duola') @observer
export default class Welcome extends Component{
  constructor() {
    super()
    this.state = {
      screenSaver: ''
    }
  }

  componentDidMount(){
    this.setState({
      screenSaver: this.props.duola.activity.screenSaver
    })
    timer = setInterval(async()=>{
      let { data } = await $.get(`http://devapi.duoladayin.com:8040/terminal/upgrade.json`);
      if(version != data['brother'].code || window.getComputedStyle(document.body,null).backgroundColor != 'rgb(254, 232, 0)'){
          location.href=`/`
      }
      this.setState({
        screenSaver: this.props.duola.activity.screenSaver
      })
    },1000 * 60)
  }

  componentWillUnmount(){
    timer && clearInterval(timer)
  }

  render(){
    return (
      <div className={style['container']} onClick={this.goHome}>
        <Header hasBack={false} hasShadow={false} hasHelp={false}/>
        <div className={style.content}>
            <img src={this.state.screenSaver} className={style.screensaver}/>
            <a className={style.tip}>
              {this.props.duola.isPrinterReady ? '轻触屏幕开始打印' : '机器维护中'}
            </a>
        </div>
      </div>
    )
  }

  goHome(){
    hashHistory.goBack()
  }
}
