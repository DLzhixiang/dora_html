import React, { Component } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; // ES6
import {Motion, spring, StaggeredMotion,TransitionMotion} from 'react-motion';


export default class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {items: [{key: 'a', size: 10}, {key: 'b', size: 20}, {key: 'c', size: 10}]};
  }

  willLeave() {
    // triggered when c's gone. Keeping c until its width/height reach 0.
    return {width: spring(800), height: spring(800)};
  }

  componentDidMount() {
    this.setState({
      items: [{key: 'a', size: 10}, {key: 'b', size: 20}], // remove c.
    });
  }
  render() {
    const items = this.state.items.map((item, i) => (
      <div key={i} onClick={() => this.handleRemove(i)}>
        {item}
      </div>
    ));

    return (
      <div style={{textAlign: 'center'}}>
      <TransitionMotion
        willLeave={this.willLeave}
        styles={this.state.items.map(item => ({
          key: item.key,
          style: {width: item.size, height: item.size},
        }))}>
        {interpolatedStyles =>
          // first render: a, b, c. Second: still a, b, c! Only last one's a, b.
          <div>
            {interpolatedStyles.map(config => {
              return <div key={config.key} style={{...config.style, border: '1px solid'}} />
            })}
          </div>
        }
      </TransitionMotion>
      </div>
    );
  }
}
