
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import Modal from 'react-modal'
import { trim } from '../../utils'
import Header from '../../components/Header'
import Back from '../../components/Back'

import Download from '../../components/Download'

let timeout  = null;

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
  }

  componentDidMount() {
    timeout = setTimeout(() => {
      hashHistory.push('/')
    }, 30000);
  }

  componentWillUnmount() {
    timeout && clearTimeout(timeout)
  }

  handlerFeedback = (value) =>{
    hashHistory.push({ pathname:'/feedback', state: value });
  }

  render(){
    return(
      <div className="container">
        <Header title = '评价成功'/>
        <div className={style['content']}>
        <img src="http://duoladayin.com:8070/image/finish.png" className={style.finish}/>
        <p className={style.text}>感谢您的评价，多拉会更加努力～</p>
        <Download />
        <Back home/>
        </div>
      </div>
    )
  }
}
          // onRequestClose={()=>{this.props.user.setLoginVisible(false)}}
