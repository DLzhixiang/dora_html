
import React, { Component } from 'react'
import { Table } from 'antd';
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import style from './style.scss'
import Back from '../../components/Back'

@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      lists: []
    }
    this.columns = [{
        title: '时间',
        dataIndex: 'time',
        width: 110
      }, {
        title: '类型',
        dataIndex: 'type',
        filters: [{
            text: 'normal',
            value: 'normal',
          }, {
            text: 'error',
            value: 'error',
          }],
          onFilter: (value, record) => record.type.indexOf(value) === 0,
          width: 60
      },{
        title: '描述',
        dataIndex: 'data',
      }];
  }

  componentDidMount(){
    this.props.duola.log.getAll().then( lists => {
        this.setState({
            lists
        })
    })
  }

  render(){
    return(
      <div className={style['container']}>
        <Table columns={this.columns} dataSource={this.state.lists.reverse()} onChange={()=>null} pagination={{ pageSize: 1000 }} scroll={{ y: '70vh' }}/>
        <Back />
      </div>
    )
 }
}
