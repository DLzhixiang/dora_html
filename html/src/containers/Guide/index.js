
import React, { Component } from 'react'
import style from './style.scss'
import { inject, observer } from 'mobx-react'
import { Link, hashHistory } from 'react-router'
import Step from '../../components/Step'
import Back from '../../components/Back'
import NProgress from 'nprogress'

import Header from '../../components/Header'

let timer = null;
@inject('duola') @observer
export default class  extends Component {
  constructor(props){
    super(props)
    this.state = {
      countDown: 60,
      status: 2
    }
    this.message = [
      "怎么了，USB设备无法被正常识别？试试重新插入？",
      "抱歉，该u盘格式不可读取，谢谢！",
      `使用说明：\n
        1.在插入USB设备后，请耐心等待5-10秒。\n
        2.系统在成功识别USB设备将自动进入文件选择界面\n
        3.多拉暂时还不支持带有加密功能的USB设备`,
       "拔出U盘，才开始打印哦～"
    ]
  }

  componentDidMount(){
    NProgress.start()
    this.onCountDown()
    if(this.props.location.state == "begin"){
      this.props.duola.onSendMessage('USBDETECTOR')
    }
  }

  onCountDown(){
    const { USBStatus } = this.props.duola;
    if(this.props.location.state == "begin"){
      timer = setInterval(()=>{
        this.setState({
          countDown: --this.state.countDown
        })
        if(this.state.countDown == 30){
            this.setState({
              status: 0
            })
        }else if (this.props.duola.USBStatus == 1) {
            hashHistory.push('/usbdrive')
        }else if (this.state.countDown == 0) {
            hashHistory.push('')
        }else if (USBStatus == -1) {
            this.setState({
              status: 1
            })
        }
      },1000)
    }else {
      this.setState({
        status: 3
      })
      timer = setInterval(()=>{
        if(this.props.duola.USBStatus == 0){
          hashHistory.push({ pathname:'/print', state: 'usb' });
        }
      },1000)
    }
  }

  componentWillUnmount(){
    NProgress.done();
    timer && clearInterval(timer)
  }

  render(){
    const { USBStatus } = this.props.duola;
    return(
      <div>
        {
          this.props.location.state == "begin" ?  <Header title="U盘打印" /> :
          <Header title="移除U盘" hasHelp={false} hasBack={false}/>
        }
        <div className={style['container']}>
            <img src={require('./assets/equipment.png')} width="327"/>
            <div className={style['guide-video']}>
              <div>
                <video src={require('./assets/guide.mp4')} autoPlay="autoplay" loop="loop"> </video>
              </div>
              <div className={(this.props.location.state == "begin") ? style['arrow-box'] : style['arrow-box-reverse']} >
                <div className={style.arrows} />
              </div>
            </div>
            <p style={(this.state.status == 2 || this.state.status == 3) ? {marginTop: 25} : null} className={(this.props.location.state != "begin") && style['remove-tip']}>
              {
                this.state.status == 0  ? <img src={require('./assets/info.png')} style={{position:'relative', top: 6, marginRight: 10}}/> : null
              }
              {this.message[this.state.status]}
            </p>
            {
              this.props.location.state == "begin" ? (
                <span className={style['countDown']}>
                    {this.state.countDown}s
                    <img src="http://www.duoladayin.com:8070/image/guide-dora.png" alt="" className={style['guide-dora']}/>
                </span>
              ) : null
            }
        </div>
        <Step style={{marginTop: '15vw'}} steps={['插入U盘','选择文件','打印设置','支付','完成打印']} step={this.props.location.state == "begin" ? 0 : 4}/>
        {
          this.props.location.state == "begin" ? <Back ev="取消U盘读取"/> :null
        }
      </div>
    )
 }
}
