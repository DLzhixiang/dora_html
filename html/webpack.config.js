var webpack = require('webpack')
var precss = require('precss');
var autoprefixer = require('autoprefixer');
var HtmlWebpackPlugin = require('html-webpack-plugin')
var pxtorem = require('postcss-pxtorem');
var path = require('path')

const svgDirs = [
  require.resolve('antd-mobile').replace(/warn\.js$/, ''),  // 1. 属于 antd-mobile 内置 svg 文件
  // path.resolve(__dirname, 'src/my-project-svg-foler'),  // 2. 自己私人的 svg 存放目录
];

var config = {
  devtool:'eval-source-map',
  context: path.join(__dirname, './src'),
  entry: {
    bundle: [
      // 'whatwg-fetch',
      // 'pdfjs-dist/build/pdf.worker.entry',
      './index.js',
    ],
    vendor: [
      "axios",
      "fastclick",
      "ityped",
      "jquery",
      "jquery-mousewheel",
      "lodash",
      "malihu-custom-scrollbar-plugin",
      "mobx",
      "mobx-react",
      "mockjs",
      "moment",
      "nprogress",
      "pdfjs-dist",
      "progressbar.js",
      "qrcode.react",
      "react",
      "react-addons-css-transition-group",
      "react-custom-scrollbars",
      "react-dom",
      "react-helmet",
      "react-modal",
      "react-motion",
      "react-router",
      "react-scrollbar",
      "react-slick",
      "store",
      "velocity-react",
      "whatwg-fetch"
    ]
  },
  output: {
    path: path.join(__dirname, './dist'),
    filename: 'bundle.js',
    chunkFilename: '[name].[chunkhash:8].chunk.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loaders: [ 'react-hot-loader', 'babel-loader?cacheDirectory' ]
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader',
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[local]_[name]_[hash:base64:5]!postcss-loader!sass-loader?sourceMap',
      },
      {
        test: /\.(jpe?g|png|gif|flv|mp4)$/,
        loader: 'url-loader?limit=8192',
      },
      {
        test: /\.html?$/,
        loader: 'html-loader',
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      }, {
        test: /\.(ttf|otf)(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader?limit=10000&mimetype=application/octet-stream"
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader"
      }, {
        test: /\.(svg)$/i,
        loader: 'svg-sprite-loader',
        include: svgDirs,  // 把 svgDirs 路径下的所有 svg 文件交给 svg-sprite-loader 插件处理
      },
    ]
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'vendor.js' }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development') }
    }),
    new webpack.LoaderOptionsPlugin({
      test: /\.(scss|css)$/,
      debug: true,
      options: {
        context: __dirname,
        postcss: function() {
            return [
              precss,
              // pxtorem({ rootValue: 100, propWhiteList: [] }),
              autoprefixer({
                browsers: ['last 2 versions', 'Firefox ESR', '> 1%', 'ie >= 8', 'iOS >= 8', 'Android >= 4'],
              })
            ];
        },
        // context: path.join(__dirname, 'src'),
        // output: {
        //     path: path.join(__dirname, 'dist')
        // }
      }
    }),
    new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|ko|ja|zh-cn)$/),
    new HtmlWebpackPlugin({ hash: false, template: "./index.ejs" })
  ],
  resolve: {
    mainFiles: ["index.web","index"],
    modules: ['node_modules', path.join(__dirname, '../node_modules')],
    extensions: ['.web.js', '.js', '.json'],
  },
  devServer: {
    host:'0.0.0.0',
    contentBase: './src',
    hot: true,
    disableHostCheck: true
  }
}

module.exports = config;
